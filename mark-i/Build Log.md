# Raiden

![](raiden-lightning-mortal_kombat_x_wallpaper.jpg)

## Physical Configuration

| Model |  Host | Port1 | Port2 | Status |
|-------|-------|:-----:|:-----:|--------|
| DL360 | KGL-PIHOLE | 34 | ?? | In-use |
| DL360 | KGL-PLEX | 36 | ?? | In-use |
| DL360 | RAIDEN-05 | 38 | ?? | Hardware untested (unplugged?) |
| DL360 | RAIDEN-04 | 40 | ?? | Hardware untested (unplugged?) |
| DL360 | RAIDEN-03 | 42 | ?? | Boots, unconfigured |
| DL360 | RAIDEN-02 | 44 | ?? | Boots, unconfigured |
| DL360 | RAIDEN-01 | 46 | ?? | Boots, unconfigured |
| DL360 | RAIDEN-FE | 48 | 24 | Boots, unconfigured | 2x3Ghz CPU, 8GB RAM

*Note: KGL-PIHOLE and KGL-PLEX will be migrated to different hardware as the cluster rolls out.*


## Applications

*  http://wwwmpa.mpa-garching.mpg.de/gadget/
*  http://nbody.sourceforge.net/
*  https://en.wikipedia.org/wiki/OpenACC
*  https://www.top500.org/news/chinese-supercomputer-runs-record-breaking-simulation-of-universe/


## Build Log

### 08032017

Decided to use ROCKS (6.2) to build the cluster.  Downloaded images, burned CD's and selected a DL360 to use as the front-end.  Figured out the tutorial was outdated and now the setup has a GUI which requires a mouse (maybe there is a work-around?).  Once the rest of the images are burned I'll track down a mouse and try the setup again.

#### Media Status

| Roll | Downloaded | Burned |
|------|:----------:|:------:|
| Kernel/Boot roll  | X | X |
| Base Roll         | X | X |
| OS Roll - disk1   | X | X |
| OS Roll - disk2   | X | X |
| OS Roll - disk3   | X | X |
| hpc               | X | X |
| ganglia           | X | X |
| perl              | X | X |
| python            |   |   |
| torque            |   |   |
| web-server        | X | X |

I either need to configure a VLAN for the cluster switch ports for each node (eth1 for RAIDEN-FE, eth0 for the rest) or I need to pick-up a separate dedicated switch for the cluster.  In the long run I'll do the latter (if only because my current switch is only 100 megabit) but I'm trying to make do with what I have before purchasing more hardware.

I'm starting to collect a list of applications to experiment with once the cluster is online.


### 08042017

#### Why?

Why am I building this?  There's a lot of reasons that boil down to "learning experience", but I have something slightly more ambitious in mind, and it's something I haven't exactly seen anyone else working on.

During a conversation about building a similar system out of Raspberry Pi's I was asked if there were any practical applications for such a computer beyond education.  My immediate answer was "probably not", since there are inherent limitations in the Raspberry Pi architecture (notably the speed of the network interface) that would prevent it from challenging systems made from more formidable components.  Furthermore, the number of nodes required to build a supercomputer capable of doing the kind of work considered "practical" would be a lot larger than most people in the Raspberry Pi audience would be willing to construct.

But then I thought about it some more an realized that I really don't know how the performance of a four-core ARM chip like that in the Raspberry Pi 3 compares to something typically used to build a large cluster such as the Xeon-based servers I'm using for Raiden.  

So aside from gaining knowledge and experience in building & operating a supercomputer, I'm going to use this project to do some direct comparisons between traditional server-based clusters and less-expensive, lower-power-consumption SBC-based designs.  The methodology for these comparisons will be somewhat informal but I think the findings will be useful in determining how to design a lower-cost, lower power consumption supercomputer architecture.  This new architecture could be explored and scaled-up to potentially achieve the performance necessary	 to take-on practical workloads.

I believe that the availability of a supercomputer like this will become valuable as the number of high-performance computing applications has been increasing, not decreasing during the last decade.  As the demand for these applications grow, the performance limits of cloud-based clusters will become more apparent and the need for dedicated supercomputer hardware will increase.  By developing an architecture which reduces both purchase and operating cost (as well as reducing harm to privacy and the environment) I may be in a unique position to serve this ever-increasing audience.

### 08062017

#### VLAN configuration

Decided to try using the switch I have instead of spending money before I need to.  Configuring my switch (Catalyst 3500XL) is a little different from the documentation I could find online.

telnet 192.168.1.255
<auth prompt>
enable
<auth prompt>
vlan database
show
vlan 100
vlan 100 name raiden

conf t
interface FastEthernet0/48
switchport mode access
switchport access vlan 100

<repeat above for ports 34-48>

#### Front-end install

Front-end LAN IP: 192.168.1.128
user: root
pass: overkill


to forward the ganglia web interface:
sudo ssh -L 80:localhost:80 root@192.168.1.128


### 08272017

#### Learning MPI

I did a bunch of work that didn't make it into the log a few weeks back, but no real progress.  I spent an evening trying to get linpack to run and ended up with nothing to show for it but a segfault.  Now that I have a little time to try again, I'm going to start over and learn how to write and run programs on the cluster before trying to get something more complex to work.

Based on a couple of examples I wrote a simple MPI program to see if I could actually run a program across the two currently active nodes in the cluster.

````
#include <stdio.h>
#include <mpi.h>

int main(int argc, char **argv)
{
        int rank, size;
        char processor_name[MPI_MAX_PROCESSOR_NAME];
        int name_len;

        MPI_Init(&argc, &argv);

        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        MPI_Get_processor_name(processor_name, &name_len);

        printf("I am MPI process %d of %d on processor %s\n", rank, size, processor_name);

        MPI_Finalize();

        return 0;
}

````

I compiled this with:

`mpicc hello.c`

...and then distributed it to the other node with:

`scp ./a.out compute-0-0.local:/home/jason/`

I then created a rankmap file containing the following:

````
raiden.local slots=2 max_slots=2
compute-0-0.local slots=4
````

And finally ran it using mpirun:

`mpirun --hostfile ./rankmap -np 8 /home/jason/a.out`

The output:

````
I am MPI process 0 of 8 on processor raiden.2soc.net
I am MPI process 1 of 8 on processor raiden.2soc.net
I am MPI process 2 of 8 on processor compute-0-0.local
I am MPI process 3 of 8 on processor compute-0-0.local
I am MPI process 5 of 8 on processor compute-0-0.local
I am MPI process 6 of 8 on processor compute-0-0.local
I am MPI process 7 of 8 on processor compute-0-0.local
I am MPI process 4 of 8 on processor compute-0-0.local
````

proved that I was able to sucessfuly run a job across the two nodes in the cluster, which was reasuring.

So now what's next is to create a more useful demonstration program (something that actually *does* something).

#### References

*  http://mpitutorial.com/tutorials/mpi-send-and-receive/
*  https://www.cs.princeton.edu/picasso/seminarsS04/MPI_Day2.pdf
*  https://www.ibm.com/developerworks/library/l-cluster2/index.html
*  https://docs.oracle.com/cd/E19708-01/821-1319-10/ExecutingPrograms.html


### 08282017

#### Adding nodes

In order to add nodes I need to boot each server and do a little configuration.  The boot order needs to be changed so they will boot of the disk array first, otherwise when they boot from CD for setup they just go into a reboot loop.  

So today I'm going through and booting each node to change the boot order and wipe the array, then booting from CD and inserting them into the cluster.

I noticed that if the keyboard and monitor are connected when a boot a node from CD, it goes into what looks like front-end setup.  What's weird is that even if I try to force it to use the "client" option at boot, there isn't one.  If I disconnect the monitor it seems to do the setup correctly, so I'm doing that on node 3 now.

Then I noticed something weird but also cool.  When I was configuring node 4 I let it boot after I wiped the disk array thinking it would just halt because I didn't have the kernel CD in the drive (it was still being used by node 3).  However when I turned around, I saw that it was loading the kickstart file.  My only guess is that it netbooted itself from the front-end automatically.  I didn't expect this, but if this is how it work, that's excellent.  I'm going to try the same steps with node 5 and see what happens.

This time I watched the new node boot and it booted from the network and began to set itself up.  This is awesome because it means I can set these things up a lot faster, but I need to go back and check on what happened to the one I tried to setup using the CD (node 3).


Then the lights went out...Apparently the time has come to put the cluster on a dedicated circuit breaker.  A little amature work and I'm ready to assess the damage.  

Since some nodes were mid-install when the power went out I'm not sure what kind of shape they are in.  The front-end seems fine, so I think I'm going to wipe and re-setup the remaining nodes.  This will give me a chance to make sure the hardware is all configured the same, and also make sure they're not set to auto-power on.

After booting node 5 another blackout.  This time it wasn't the breaker box but probably the breaker in one of the power strips I'm using to distribute power to the cluster (I'm using two in series).  This can probably be solved by not ganging the two strips together, but I'm not excited about having to start over a third time at the moment so I think it's time for a break.


### 08292017

After giving it some thought (and recalculating power requirements) I think I'm goign to scale-back the cluster to 5 nodes (1 front-end and 4 compute).  This should still be large enough to perform the experiments I want while small enough I should be able to run it without adding new circuits to the lab.

This will also reduce the cost of the overall project both in terms of power consumption and new hardware when the time comes to build Mark II.

Did a little electrical work and now I have four compute nodes on a dedicated 15A circuit and one front-end on a separate circuit.  Working on booting things up now to see what condition the selected nodes are in after last night's power fault.

One of the nodes boots correctly, but the rest need to be re-installed.  Two do this automatically, one has a borked GRUB so I need to blow the disks away first.  The good news is that Rocks figures out something is wrong and re-installs the nodes automatically which is very handy.

I get almost all the nodes back online but during setup on the fourth compute node the power goes out again.  It only impacts the four compute nodes and doesn't throw the house's breaker, so it's got to be something about the power strip.  I can't find any rating on it, so my next step will be to replace it with something I know can handle 15A.

The power strip reset itself and all four compute nodes roared to life.  I figured it would trip again but it didn't, and the four re-installed their software and came on-line after about 15-20 minutes.  At this point all five nodes are operational, although I don't know for how long.

I was able to run a few of my MPI programs on the cluster to make sure jobs were getting distributed correctly and everything looks good.  I was even able to get some load to show up on the graphs with the `reduce_stddev` program.

I'm not sure what to immediately do next.  I wrote a blog post about the current status, and I think the next thing I want to do is either get LINPACK running or get some other more interesting software going on the cluster (maybe an n-body simulation).  I don't want to invest too much time before I find a replacement for the power strip though because I think I'm probably on borrowed-time already.

I also need to configure the VPN so I can connect to the cluster from elsewhere.


### 08302017

Taking a whack at running some more interesting applications on Raiden beginning with some n-body simulation using GADGET: http://wwwmpa.mpa-garching.mpg.de/gadget/.

As always the primary challenge is getting it to compile.  After settling on an `-march` setting that worked (`native`) I fought with the other dependencies.  Ultimately I ended up compiling the dependencies from source, specifying where they went based on this random page:

https://github.com/votca/csg/wiki/Installing

Looks like the documentation for Gadget is out of date (or at least refers to older versions of its dependencies).  I spent a fair amount of time reading the documentation of the various libraries to figure out how to compile them such that the output looked like what the Gadget code was expecting.


All this makes me realize that a lot of the work that goes into doing something useful with a supercomputer like this is sysadmin, and if I want to design a system that can be put to work by users it would be good to focus on delivering an out-of-the-box experience that has a lot of this typical legwork done.  I suppose this is what companies like Cray, IBM, etc. spend a lot of their time on.

After a few hours of dorking around with compilation, I was simulating a galaxy collision...

````
[jason@raiden Gadget-2.0.7]$ mpirun -np 8 ./Gadget2/Gadget2 ./Gadget2/parameterfiles/galaxy.param 

This is Gadget, version `2.0'.

Running on 8 processors.

Allocated 25 MByte communication buffer per processor.

Communication buffer has room for 284938 particles in gravity computation
Communication buffer has room for 109226 particles in density computation
Communication buffer has room for 86231 particles in hydro computation
Communication buffer has room for 81920 particles in domain decomposition


Hubble (internal units) = 0.1
G (internal units) = 43007.1
UnitMass_in_g = 1.989e+43 
UnitTime_in_s = 3.08568e+16 
UnitVelocity_in_cm_per_s = 100000 
UnitDensity_in_cgs = 6.76991e-22 
UnitEnergy_in_cgs = 1.989e+53 

Task=0  FFT-Slabs=16
Task=1  FFT-Slabs=16
Task=2  FFT-Slabs=16
Task=3  FFT-Slabs=16
Task=4  FFT-Slabs=16
Task=5  FFT-Slabs=16
Task=6  FFT-Slabs=16
Task=7  FFT-Slabs=16

Allocated 1.54495 MByte for particle storage. 144


reading file `ICs/galaxy_littleendian.dat' on task=0 (contains 60000 particles.)
distributing this file to tasks 0-7
Type 0 (gas):          0  (tot=     0000000000) masstab=0
Type 1 (halo):     40000  (tot=     0000040000) masstab=0.00104634
Type 2 (disk):     20000  (tot=     0000020000) masstab=0.00023252
Type 3 (bulge):        0  (tot=     0000000000) masstab=0
Type 4 (stars):        0  (tot=     0000000000) masstab=0
Type 5 (bndry):        0  (tot=     0000000000) masstab=0

reading done.
Total number of particles :  0000060000

allocated 0.0762939 Mbyte for ngb search.

Allocated 1.81018 MByte for BH-tree. 112
````

...unfortunately this was only running on one node, the front-end.  Fortunately I think I know how to fix that by using the `rankmap` file.  This almost worked, except that the library path on the other nodes was still missing the path to the gsl libraries.

```
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/share/apps/gsl/lib/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/share/apps/fftw/lib/
export LD_LIBRARY_PATH
````
After learning how to configure the LD_LIBRARY_PATH globally (create a file in `/etc/ld.so.conf.d/` containing the paths and then run `ldconfig`), the job kicked-off without an error.

```
[jason@raiden Gadget-2.0.7]$ mpirun --hostfile ./rankmap -np 8 ./Gadget2/Gadget2 ./Gadget2/parameterfiles/galaxy.param 

This is Gadget, version `2.0'.

Running on 8 processors.

Allocated 25 MByte communication buffer per processor.

Communication buffer has room for 284938 particles in gravity computation
Communication buffer has room for 109226 particles in density computation
Communication buffer has room for 86231 particles in hydro computation
Communication buffer has room for 81920 particles in domain decomposition


Hubble (internal units) = 0.1
G (internal units) = 43007.1
UnitMass_in_g = 1.989e+43 
UnitTime_in_s = 3.08568e+16 
UnitVelocity_in_cm_per_s = 100000 
UnitDensity_in_cgs = 6.76991e-22 
UnitEnergy_in_cgs = 1.989e+53 

Task=0  FFT-Slabs=16
Task=1  FFT-Slabs=16
Task=2  FFT-Slabs=16
Task=3  FFT-Slabs=16
Task=4  FFT-Slabs=16
Task=5  FFT-Slabs=16
Task=6  FFT-Slabs=16
Task=7  FFT-Slabs=16

Allocated 1.54495 MByte for particle storage. 144


reading file `ICs/galaxy_littleendian.dat' on task=0 (contains 60000 particles.)
distributing this file to tasks 0-7
Type 0 (gas):          0  (tot=     0000000000) masstab=0
Type 1 (halo):     40000  (tot=     0000040000) masstab=0.00104634
Type 2 (disk):     20000  (tot=     0000020000) masstab=0.00023252
Type 3 (bulge):        0  (tot=     0000000000) masstab=0
Type 4 (stars):        0  (tot=     0000000000) masstab=0
Type 5 (bndry):        0  (tot=     0000000000) masstab=0

reading done.
Total number of particles :  0000060000

allocated 0.0762939 Mbyte for ngb search.

Allocated 1.81018 MByte for BH-tree. 112


```

Since I only sent the job to 8 processors only two of the four compute nodes started lighting-up.  I'm considering restarting the job with 16 but given the power distribution problems I've had I might hold-off on that for a bit.

After watching the galaxy example run for about an hour I wasn't seeing any output and started to wonder what to expect.  I found a smaller example (just 1472 particles) so I stopped the first one and started this second one, this time with 16 processors.  Now the cluster is really cranking, so we'll see how long it takes for it to finish this off.


### 09052017

Did a little poking around for an Altair-style chassis for Mark II and found this:

http://www.ebay.com/itm/310mmx285mmx115mm-Electronic-Project-Junction-Box-Enclosure-Case-Blue/352093203103?_trkparms=aid%3D222007%26algo%3DSIM.MBE%26ao%3D2%26asc%3D46086%26meid%3Df557342cf20e407e88edeeda436d4957%26pid%3D100005%26rk%3D6%26rkt%3D6%26sd%3D222466385463&_trksid=p2047675.c100005.m1851

Looks like there are a lot of cases of this type available in various sizes.  It's not *exactly* what I want but it is incredibly close, reasonably priced and requires zero fabrication (at least for the exterior of the case).

I'm going to cut-up some cardboard to model the size of the Ethernet switch, power supply & PINE64 boards and see which of these cases would be the best file while approximating the aesthetic of the Altair.


### 10062017

Had to shut the cluster down for a few weeks due to ambient temperatures causing the lab to overheat.  Now that things have cooled-down it's possible to resume work establishing baseline performance of the Intel-based cluster.

While devizing custom MPI benchmarks was a good learning experience (learning MPI has long-term practical advantages), getting a solid LINPACK measurement is the best way to get an apples-to-apples comparison between Raiden MARK I and future incarnations.  So today I'm turning my attention to getting the LINPACK benchmark running on Raiden Mark I.

References for building HPL the "hard" way:

*  https://www.rit.albany.edu/wiki/Benchmarking_with_LINPACK/HPL
*  http://www.advancedclustering.com/act_kb/tune-hpl-dat-file/
*  https://www.tacc.utexas.edu/research-development/tacc-software/gotoblas2
*  http://www.netlib.org/benchmark/hpl/tuning.html
*  http://www.netlib.org/utk/people/JackDongarra/faq-linpack.html
*  https://frankinformation.wordpress.com/2013/09/09/installing-linpack-benchmark-hpl-on-a-rocks-cluster/

Finally following the directions in the last link above, I got HPL to compile & run!  Now to wait for results...


After a few minutes the following results were produced:

```
[linpacker@raiden Linux_PII_FBLAS]$ mpirun -np 4 -machinefile /home/linpacker/machines xhpl
================================================================================
HPLinpack 2.1  --  High-Performance Linpack benchmark  --   October 26, 2012
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   29184 
NB     :     192 
PMAP   : Row-major process mapping
P      :       2 
Q      :       2 
PFACT  :   Right 
NBMIN  :       4 
NDIV   :       2 
RFACT  :   Crout 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0



================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR11C2R4       29184   192     2     2             692.41              2.393e+01
HPL_pdgesv() start time Fri Oct  6 12:58:50 2017

HPL_pdgesv() end time   Fri Oct  6 13:10:22 2017

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0011543 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
```

I'm not sure how to interpret this yet.  I think the Glops number is what's referred to as the Rmax in the Top 500 list but I'm not sure.

Aside from figuring out how to read the results, the next step will be to fire-up a few more nodes and run the benchmark across the cluster instead of just the head node.

I fired up four more nodes to run a second test.  Unfortunately two didn't join the cluster (maybe they will heal themselves) but that was OK since I just wanted to see what two nodes looked like vs. one.

This test ran on compute-0-0 and compute-0-1 and I meante to leave the head node out but forgot to be explicit.  Regardless it didn't seem to absorb much load so maybe it was left out anyway.  After 10 minutes or so the resuls were in and I have to say they were a little dissapointing:

```
[linpacker@raiden Linux_PII_FBLAS]$ mpirun -np 8 -machinefile /home/linpacker/machines xhpl  
Could not create directory '/home/linpacker/.ssh'.
Warning: Permanently added 'compute-0-0' (RSA) to the list of known hosts.
Warning: Permanently added 'compute-0-1' (RSA) to the list of known hosts.
================================================================================
HPLinpack 2.1  --  High-Performance Linpack benchmark  --   October 26, 2012
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   29184 
NB     :     192 
PMAP   : Row-major process mapping
P      :       2 
Q      :       2 
PFACT  :   Right 
NBMIN  :       4 
NDIV   :       2 
RFACT  :   Crout 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR11C2R4       29184   192     2     2             589.34              2.812e+01
HPL_pdgesv() start time Wed Jan 23 18:08:43 2008

HPL_pdgesv() end time   Wed Jan 23 18:18:32 2008

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0009535 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
```

Not much improvement in the Gflops.  I didn't make any changes so maybe the HPL.dat needs to be tweaked.  Let's try using the HPL generator set to two nodes and run again.


```
[linpacker@raiden Linux_PII_FBLAS]$ mpirun -nolocal -np 8 -machinefile /home/linpacker/machines xhpl
================================================================================
HPLinpack 2.1  --  High-Performance Linpack benchmark  --   October 26, 2012
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   41088 
NB     :     192 
PMAP   : Row-major process mapping
P      :       2 
Q      :       4 
PFACT  :   Right 
NBMIN  :       4 
NDIV   :       2 
RFACT  :   Crout 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR11C2R4       41088   192     2     4            1333.90              3.467e+01
HPL_pdgesv() start time Wed Jan 23 18:25:19 2008

HPL_pdgesv() end time   Wed Jan 23 18:47:33 2008

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0007763 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================

```

... clearly tuning HPL.dat makes a significant difference.

I'll have to figure out why the other two nodes are AWOL and then invest some time tuning the HPL.dat for a four-node system, but I think I'm well on my way to esablishing a baseline for the Intel-based cluster.  


### 10092017

Started experimenting with running Linpack on PINE64 by reformatting my Pinebook.  Here's the setup:

1. Install xenial minimal
2. Manually configure wifi
3. Build & install MPI as per: https://likymice.wordpress.com/2015/03/13/install-open-mpi-in-ubuntu-14-04-13-10/
4. Build & install Linpack per: https://frankinformation.wordpress.com/2013/09/09/installing-linpack-benchmark-hpl-on-a-rocks-cluster/


### 10112017

Bought another VGA cable (forgot that I put the one I used for the cluster into one of the arcade machines) and was able to boot compute-0-2 to figure out why it wasn't joining the cluster.  Looks like the local filesystem got damaged so I wiped the array and started over.  For some reason it wouldn't repair itself so I pulled it from the cluster using 

`insert-ethers --remove compute-0-1`

This did the trick and after rebooting it showed up in `insert-ethers` however it took the name `compute-0-4` which is bound to be confusing down the road.  My guess as to why this is is because I didn't remove `compute-0-3` yet (trying to keep troubleshooting focused on one node at a time) so I might blow both away again and start over just so the naming remains sane.

After blowing-away `compute-0-3` and`compute-0-4` and replacing the power strip with one rated for 15A all four compute nodes were re-installed and online.

I left the cluster alone to idle for about half an hour to make sure it was stable and then prepped for another Linpack run.  Here's the results:

```
[linpacker@raiden Linux_PII_FBLAS]$ mpirun -nolocal -np 16 -machinefile /home/linpacker/machines xhpl                                                                       
================================================================================      
HPLinpack 2.1  --  High-Performance Linpack benchmark  --   October 26, 2012          
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   58368 
NB     :     192 
PMAP   : Row-major process mapping
P      :       4 
Q      :       4 
PFACT  :   Right 
NBMIN  :       4 
NDIV   :       2 
RFACT  :   Crout 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR11C2R4       58368   192     4     4            3618.70              3.664e+01
HPL_pdgesv() start time Mon Jan 28 17:02:57 2008

HPL_pdgesv() end time   Mon Jan 28 18:03:16 2008

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0005444 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================

```

I was hoping to see a more significant improvement in performance since I'm doubling the number of processors, but maybe I need to spend some time hand-tuning the HPL.dat file or experimenting with different configurations for the run.  Based on the graphs network utilization is nowhere near capacity and system load is under 75%, so there should be some room for improvement to squeeze the most out of the hardware.

Looking closer at the graphs, I think the reason it looks like the benchmark isn't utilizing the cluster 100% is because I'm leaving the head node out of the job, but the charts count its capacity when calculation utilization percentages.

For kicks I did another run with 20 processses instead of 16:

```
[linpacker@raiden Linux_PII_FBLAS]$ mpirun -nolocal -np 20 -machinefile /home/linpacker/machines xhpl
================================================================================
HPLinpack 2.1  --  High-Performance Linpack benchmark  --   October 26, 2012
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   58368 
NB     :     192 
PMAP   : Row-major process mapping
P      :       4 
Q      :       4 
PFACT  :   Right 
NBMIN  :       4 
NDIV   :       2 
RFACT  :   Crout 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR11C2R4       58368   192     4     4            3206.66              4.134e+01
HPL_pdgesv() start time Mon Jan 28 18:15:55 2008

HPL_pdgesv() end time   Mon Jan 28 19:09:22 2008

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0006339 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
```

Surprisingly this made a measurable improvment in the Gflops measurement.  I found a link that shows some results from other small clusters and based on these results I think there is room for improvment if I can figure out how to tune things correctly.

http://www.netlib.org/benchmark/hpl/results.html

I made a few guesses at different values in the HPL.dat and performance took a considerable jump:

```
[linpacker@raiden Linux_PII_FBLAS]$ mpirun -nolocal -np 16 -machinefile /home/linpacker/machines xhpl
================================================================================
HPLinpack 2.1  --  High-Performance Linpack benchmark  --   October 26, 2012
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   20000 
NB     :      32 
PMAP   : Row-major process mapping
P      :       4 
Q      :       4 
PFACT  :   Right 
NBMIN  :       4 
NDIV   :       2 
RFACT  :   Crout 
BCAST  :  BlongM 
DEPTH  :       0 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR05C2R4       20000    32     4     4             594.99              8.965e+00
HPL_pdgesv() start time Mon Jan 28 19:37:08 2008

HPL_pdgesv() end time   Mon Jan 28 19:47:03 2008

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0007882 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
```

Here's the settings that produced the results above:

```
HPLinpack benchmark input file
Innovative Computing Laboratory, University of Tennessee
HPL.out      output file name (if any)
6            device out (6=stdout,7=stderr,file)
1            # of problems sizes (N)
20000         Ns
1            # of NBs
32           NBs
0            PMAP process mapping (0=Row-,1=Column-major)
1            # of process grids (P x Q)
4            Ps
4            Qs
16.0         threshold
1            # of panel fact
2            PFACTs (0=left, 1=Crout, 2=Right)
1            # of recursive stopping criterium
4            NBMINs (>= 1)
1            # of panels in recursion
2            NDIVs
1            # of recursive panel fact.
1            RFACTs (0=left, 1=Crout, 2=Right)
1            # of broadcast
5            BCASTs (0=1rg,1=1rM,2=2rg,3=2rM,4=Lng,5=LnM)
1            # of lookahead depth
0            DEPTHs (>=0)
2            SWAP (0=bin-exch,1=long,2=mix)
64           swapping threshold
0            L1 in (0=transposed,1=no-transposed) form
0            U  in (0=transposed,1=no-transposed) form
1            Equilibration (0=no,1=yes)
8            memory alignment in double (> 0)
```

More experimentation is in order...

I noticed that I was misreading the network graph (reading bytes as bits) and it looks like we could be bottlenecking at the switch.  I'm going to keep an eye on this but it might make sense to order the gigabit ethernet switch for Mark II and try it out with the Mark I hardware to see if that changes things substantially.


### 10122017

Here's the best results I've been able to get so far:

```
[linpacker@raiden Linux_PII_FBLAS]$ mpirun -nolocal -np 16 -machinefile /home/linpacker/machines xhpl
================================================================================
HPLinpack 2.1  --  High-Performance Linpack benchmark  --   October 26, 2012
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   20000 
NB     :     128 
PMAP   : Row-major process mapping
P      :       4 
Q      :       4 
PFACT  :   Right 
NBMIN  :       4 
NDIV   :       2 
RFACT  :   Crout 
BCAST  :  BlongM 
DEPTH  :       1 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR15C2R4       20000   128     4     4             567.27              9.403e+00
HPL_pdgesv() start time Tue Jan 29 19:22:24 2008

HPL_pdgesv() end time   Tue Jan 29 19:31:51 2008

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0009847 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================

```

Here's the HPL.dat settings that produced the results above:

```
HPLinpack benchmark input file
Innovative Computing Laboratory, University of Tennessee
HPL.out      output file name (if any)
6            device out (6=stdout,7=stderr,file)
1            # of problems sizes (N)
20000         Ns
1            # of NBs
128           NBs
0            PMAP process mapping (0=Row-,1=Column-major)
1            # of process grids (P x Q)
4            Ps
4            Qs
16.0         threshold
1            # of panel fact
2            PFACTs (0=left, 1=Crout, 2=Right)
1            # of recursive stopping criterium
4            NBMINs (>= 1)
1            # of panels in recursion
2            NDIVs
1            # of recursive panel fact.
1            RFACTs (0=left, 1=Crout, 2=Right)
1            # of broadcast
5            BCASTs (0=1rg,1=1rM,2=2rg,3=2rM,4=Lng,5=LnM)
1            # of lookahead depth
1            DEPTHs (>=0)
2            SWAP (0=bin-exch,1=long,2=mix)
64           swapping threshold
0            L1 in (0=transposed,1=no-transposed) form
0            U  in (0=transposed,1=no-transposed) form
1            Equilibration (0=no,1=yes)
8            memory alignment in double (> 0)
```

I ordered a gigabit switch because it looks like I'm maxxing-out the fast ethernet switch and I'm curious to see if adding headroom to the network can push the cluster past the 10 Gflops mark.




### 10232017

Re-running HPL on a single node (the head node) to see what kind of performance is possible with a single computer and possibly rule-out network


### 11052017

After some fiddling around (which I really need to document later) I'm running HPL on an ARM machine very simular to one of the nodes I'll be building into Raiden Mark II.  My Pinebook laptop has essentially the 2GB version of the PINE A64+ board inside it so other than having more RAM it should be very close in terms of performance to the 1GB boards I'll be using in Mark II.  

Now that I've build HPL a few times I'm starting to understand the various dependencies and configuration files and things went much smoother this time.  I used ATLAS to build the linear algebra library this time but I'm still using openmpi & the mpicc compiler like Mark I.

Side note: I found out not only is there already a supercomputer named Raiden, but it's also on the Green 500 list.  This is a bummer.  I'm no sure if I'm going to rename this project or just ignore the clash until it matters.  I've become pretty attached to the name and it will probably be a few more iterations before I'll need to be concerned about name clashes on the Top500.:

First run with a non-default HPL.dat (using a generated one still) is returning 2.834Gflops.  This is encouraging (slightly better than the first single-node test I ran on Mark I!) given that it's a single four-core machine.

It will be interesting to see the performance curve as I add more ARM nodes and compare that to Mark I's performance.  Given that the best I could squeeze out of Mark I's five nodes was just under 10Gflops, it's not unimagiable, based on these initial results, that I could meet or exceed that with 7 more nodes like the one I tested tonight...

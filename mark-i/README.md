# RAIN Mark I

## Summary

The goal of Mark I was to create a traditional Linux cluster supercomputer to serve as a baseline of comparison at a scale simular to the Mark II machines.  In addition to these metrics, building Mark I helped identify the challenges of assembling, testing and operating a high-performance computing cluster.

The contents of this directory are somewhat disorganized and reflect the learning process of assembling a system such as this. [Build Log.md](Build Log.md) contains a stream-of-conciousness record of the work that went into building Mark I as well as thoughts and ideas for the overall project which came to mind during the build. 

## Status

Complete.  Even though Mark I has served its purpose, I'd like to run some additional experiments and do more accurate power consumption measurements as well. As such, I plan to keep it assembled until I can get around to that (or until someone is willing to take the hardware off my hands).

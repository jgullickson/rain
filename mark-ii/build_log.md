# 03142018

There's a lot missing from this log.  I've been documenting this project on the blog for the most part, but I need somewhere to keep track of rambling thoughts and output from experiments that's not really fit for human consumption, so I resumed the "build log" approach I used during the Mark I work.

## RAIN-PSC Milestones

When will RAIN-PSC be complete?  Well the goal is to have a self-contained personal supercomputer so a good place to start would be to describe exactly what needs to happen between where we are and that goal post.

There's a lot of interesting features I want to include in rain-psc but it's important to focus on the goals of the overall project.  Mark II's original purpose was to re-create Mark I using ARM SBC's and I think we're a lot closer to that than we are to producing the machine I've started to envion as the psc.  So instead of basing the first milestone on completing the contemporary vision of the PSC, what would it take to fulfil the original Mark II mission?

With the hardware we have:

* Connect the A64 board (front-end) to the Clusterboard via Ethernet
* Power both boards using separate, external power supplies
* Configure static IP's for the ethernet port on the front-end and the SOPINE module to create a private cluster network
* Compile & run an MPI example

This would demonstrate the basic cluster functionality of the hardware we have on-hand.  It would also make it possible to interact with the machine remotely and the serial console could be removed.

Using a setup like this it would be *possible* to acheive the original Mark II goal by procuring 6 more SOPINE modules and running linpack.  The question is if this is a better use of (primarilly financial) resources vs. completing the rest of the PSC hardware work (front panel drivers, power management, etc.)

There is some financial incentive for tackeling some more software/config tasks before purchasing more SOPINE modules.  Specifically, getting the module we have to boot from the network (using the on-board SPI flash) would eliminate the need to have SD cards for all SOPINE modules (~$60).

# 04022018

Having a little trouble getting my bearnings after returning from vacation.  I think what I want to work-on next is getting everything inside the enclosure, and that means making a front-panel driver board that can be mounted to the Clusterboard.

To that end I've purchased a few "perma-proto" boards which I think are a good compromise between using raw perfboard and waiting until I can design & fabricate a proper custom PCB for the driver boards.  This isn't something I'd want to do for all 8 nodes, but it should be good enough to get the front-end and one of the SOPINE module panel drivers up-and-running.

## LINPACK

While I'm waiting for "lab time" to do the hardware work above I decided to work on getting LINPACK running even if the hardware isn't scaled-up yet.  I'm working from [this guide](https://jahanzebnotes.blogspot.com/2013/06/how-to-run-hpl-benchmark-with-atlas.html) (which I used earlier to get it running on my Pinebook).

### Steps

1. sudo apt-get install build-essential gfortran
2. sudo apt-get install libopenmpi-dev openmpi-bin openmpi-doc
3. wget https://sourceforge.net/projects/math-atlas/files/Stable/3.10.3/atlas3.10.3.tar.bz2
4. bzip2 -d atlas3.10.3.tar.bz2
5. tar xf atlas3.10.3.tar
6. cd ATLAS, mkdir buildDir, cd buildDir
7. sudo cpufreq-set -r -g performance
8. ../configure --prefix=/home/jjg/ATLAS/buildDir/ --cripple-atlas-performance
9. make


# 04032018

Working-through getting linpack running brought-up the topic of CPU throttling so I did a little research on this (since I couldn't disable it to ATLAS's satisfaction).  In the process I stumbled-upon a number of interesting resources for tuning the ARM SOC's used by boards like these:

* https://github.com/longsleep/build-pine64-image/pull/3
* https://groups.google.com/forum/#!msg/linux-sunxi/20Ir4It3GsA/TPT9wv8IAQAJ
* https://patrickmn.com/projects/cpuburn/
* https://linux-sunxi.org/Hardware_Reliability_Tests

After reading this I'm not going to sweat tuning the hardware for ATLAS too much.  Looks like there's a lot of variation in maximum performance (in terms of clock speed) even between identical SOC models, let alone cooling, etc.  I'll leave this for later since it sounds like it will involve more than just setting the clock speed, disabling the governor and letting it rip.

Also came across some PINE64 linpack info here:

https://pastebin.com/iPkJd5Qa

Not sure what libraries, etc. are behind this but it's informative regardless.

Also found a nice summary of immersion cooling techniques (mineral oil seems viable?) which might be useful for Mark III work:

https://en.wikipedia.org/wiki/Server_immersion_cooling

# 04042018

Resuming work on getting linpack to run and spent the morning fighting a compliation problem.  Turned out to be an extra space character on the `ARCH` line of the Makefile.  I'm pretty sure I was bitten by this before (back in the Mark I days) but it still took me a few hours to trace it.  This is why I'm making sure to note it carefully here.

Once this makefile error was corrected, linpack compiled & the tests ran successfully.  Next step is getting it running on the cluster.

To do this we need to make sure the other node (only one in the cluster at the moment) has the libraries, executables, etc. we need to run linpack.  This means installing the same dependencies outlined above as well as copying-over linpack and all it's dependencies we just compiled above.  In the long-run this should be done in a more automatic fasion (perhaps NFS, etc.) but for one node we can just do it by hand.

At this point I notice that there's some network configuration missing from the cluster nodes...

I have a choice to make now.  The cluster nodes can't currently reach the public Internet, which prevents me from installing or upgrading OS components the normal way.  I can fix this, but ultimately that might not be the right way to go from a security/consistency/etc. perspective.  On the other hand, I could fix this by manually compiling the dependencies and then copy them over (like I did with linpack, ATLAS, etc.) but that's going to take some time.  I'm not sure which I want to undertake right now.

I think the only *required* dependency is OpenMPI, so I'm going to take a stab at compiling that locally on the front-end node, and then recompiling hpl/linpack to work with that.

In the long run these programs and all their dependencies should probably be compiled once on the front-end node and then distributed across the cluster via scripts or a shared network device.  Keeping the individual nodes "in the dark" reduces the chances that unexpected upgrades might change the behavior of the system, and not allowing traffic to flow to them via the front-end should mitigate most security concerns that could arise from not updating the OS on each node regularly.

Really it's possible that these nodes will not have much of a local filesystem at all in later configurations.  More likely they will boot from local SPI flash, and then mount the majority of their operating system via NFS from the front-end.  In this scenario most of the operating system (save the kernel) will be maintained as part of front-end maintenance, I would presume.

While I was waiting for openmpi to compile, I added my public key to the first cluster node.

After recompiling, I still couldn't get hpl to run across the cluster.  When I try to run it on the non front-end node, I get a missing library error:

`/home/jjg/openmpi/buildDir/bin/mpirun: error while loading shared libraries: libnuma.so.1: cannot open shared object file: No such file or directory`

The reason for this didn't jump-out at me so I started removing components from the front-end node until I could get the same error there.  After removing the openmpi packages, I got the same error.  Simply trying to run `mpirun` from the new (freshly-compiled) local directory yeilds this error so something must have been linked incorrectly.

After some poking around the missing libraries were found in the package `libnuma-dev_2.0.11-1ubuntu1.1_arm64.deb`.  Since the cluster node isn't able to reach the Internet I pulled this from the front-end's cache (`/var/cache/apt/archives`) and scp'd it over for manual installation.  This required some further dependency resolution (installing `libnuma1_2.0.11-1ubuntu1.1_arm64.deb` first) but after that I was able to execute `mpirun` without the error.


# 04052018

Did some late-night hacking on the HPL.dat file and was able to get hpl to run across both nodes in the cluster.  The best result I got was around 9 gflops, but I'm not sure how accurate that is since it was using a very small problem size.  I spent an hour or so fiddling with the configuration but managed to crash the front-end node and that was the end of my experiments.

The good news is that I have a working build and configuration for running hpl across multiple nodes in the cluster, and while the results may not reflect the best numbers possible yet, that's not the goal of this exercise.  Measuring peak performance will require a lot more tweaking and will also involve chassis work (as I believe that the cause of my crash last night may have been heat-related).  Right now I just wanted to get a working build of the software components and a configuration that can work for running jobs across the cluster.  Next steps on the software side will be to automate that work, but now I really need to focus on getting all the hardware back in the box so I can start to measure things like thermal characteristics and work with the entire machine in a more convenient package.



# 04962018

Perma-proto boards and associated connectors arrived and after doing some test-fitting, it looks like they are not going to work as easily as I had hoped.  Minimally, I'm going to have to saw-off the power bus on one side in order to use the 20-pin header the way I want to and even that might be problematic due to spacing problems.

I'm starting to accept the fact that the right way to do this is to create a custom PCB.  I was really hoping I could put this off because it's going to consume a lot of time, both because I need to learn how to use an EDA and also because of the turn-around time of getting boards made.  This is time I'd have to spend at some point, but I was just hoping to get everything back inside the case for awhile before taking this big of a bite.

In any event, I've probably wasted an equivalent amount of time trying to avoid this, so I'm going to just take the plunge.

Ran a couple hpl tests with the (breadboard-based) panel monitor on and ran into the same crash/hang situation as before.  Did a little probing and it's definately a temperature issue.  If I do to hpl runs back-to-back with np=4000, the first completes sucessfully but the second will hang the front-end node.  Watching the cpu temp, I can see it cross 80 degrees during the first run and take minutes to cool back down to ambient temp, so it figures if I did a second run immediately the temp could get pushed past 100 where the safety valves kick-in.  Running a remote (ssh) test, I stop getting a response from the board when the temp hits 80 degrees during the second run.

Upon further thought I think this is a *power* issue, not a thermal one (at least not at this stage).  The reason for this is that while the front-end node locks-up, the SOPINE module on the Clusterboard does not.  The front-end is powered through a USB power supply which are known to be insufficient for the A64 boards when they are running hard.  On the other hand the SOPINE module is powered via the Clusterboard which has a very beefy power supply.  Since one crashes and the other doesn't, I think it's safe to say it's more likely a power than a temperature thing (since both boards have simular cooling).

Based on this I'm planning to discontinue including the front-end in any hpl runs until it's got a better power supply (and honestly, if I had any more SOPINE modules I wouldn't be using it at all anyway).


# 04112018

Did some work over the weekend to develop an i2c-based driver board for the front-panel and part of that is testing to make sure the circuit I tested with the A64 board will work with the Clusterboard.  This should have been easy however I ran into software problems getting i2c to work on the SOPINE module and in the process of troubleshooting it, blew up the OS.

Today I'm starting with a fresh image and taking a more careful approach.  The first step is making it possible for the Clusterboard modules to reach the Internet.  I debated whether or not I want this to work and in the long run I'll probably disable it, but for now it's essential as I'm constantly tweaking the software installed on the SOPINE module.

To this end I configured a DHCP server on the front-end node that can hand-out both private IP addresses and a public nameserver (currently Google's 8.8.8.8).  I also configured the front-end to provide routing/forwarding/NAT for the Clusterboard nodes.  This was all done using discreed components but looking back at it I think something like dnsmasq would be a better approach.  In any event this isn't the current objective, testing the driver board is, so I'm focused on that at the moment.

Something I learned the hard way is that I can't blindly update the operating system since I'm using a patched version of Armbian that has a tweaked config to support the Ethernet bits on the Clusterboard.  So this time around I'm treading lightly as I try to get i2c working.

`sudo apt-get install i2c-tools`
`sudo i2cdetect -y 0`

returns

`Error: Could not open file `/dev/i2c-0': No such device or address`

`ls /dev/i2c* ` shows the device exists, and since I'm using `sudo` permissons shouldn't be an issue, so something else is going on.  One bit of advice I got on the PINE64 forum was to check to make sure the i2c node was enabled in the device tree.  I'm not sure how to check that so I guess the next step is to figure that out.

On a whim I tried `armbian-config` (which behaved strangely on the previous build) and it displayed the option to enable i2c (0 and 1) correctly.  This is worth a shot...

...that worked!

One issue was that it required a reboot, and for some reason the SOPINE modules in the Clusterboard don't always reboot when they are told to (this is a [known issue](https://forum.pine64.org/showthread.php?tid=5849) apparently).  This is easily remedied by pressing the reset button on the Clusterboard, but it's inconvinent when the board isn't physically nearby (and won't be an option once more than one module is installed).  Another thing I'll need to look into later..

With the config updated and the module rebooted I'm able to run `i2cdetect` without error.  Now to see if my interface is detected...

```
jjg@pine64so:~$ sudo i2cdetect -y 0
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- --                         
jjg@pine64so:~$ sudo i2cdetect -y 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- --                         

```

Unfortunately, scanning both busses returned nothing which is the same behavior I saw on the A64 before I added the 3.3v pull-up resistors.  It's possile this is the problem, which would really be a bummer because the Clusterboard's header doesn't provide 3.3v so I don't think I can't simply add a couple resistors to solve it.  It's also possible that another problem exists (incorrect wiring, something came loose, etc.) so I'll try to eliminate those possibilities first.

After checking all the wiring I bit the bullet and used a regulator (AMS1117-3.3?) to drop the 5v to 3.3v.  Tying the i2c pins to this via 200 ohm resistors, I was able to see the driver chip:

```
jjg@pine64so:~$ sudo i2cdetect -y 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: 20 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- --   
```

This feels like a hack, and there's got to be a better way to do it but it works, so I'll roll with it for now.  I'll modify the driver board schematic to include a 5v-3.3v step-down and if I figure out something better after ordering the board it should be easy to patch it in place.

Now to try running the panel driver software:

`sudo apt-get install python python-dev python-pip python-smbus`

After that I `scp`'d just the `panel.py` script over to the SOPINE (didn't want to wait for the entire repository to clone) and ran the script.  It works!


# 04132018

Discovered KiCad's "interactive router" and had more luck getting traces down on the board.  Talked about designing PCB's more abstractly with Bob and after adding a second layer and some copper fills, I was able to tweak the board to the point where it would pass the DRC's.  Not sure if this means it's "all good", but let's see if oshpark will accept it?

Board looks good on oshpark and Bob didn't find any fatal errors in the design files.  He suggested trying to fatten-up the traces to 10mil (12 for power) and that some of them are kind of crazy and long, but nothing he thought would prevent it from working.  At this point I'm tempted to just pull the trigger on the boards and see what happens, but I might at least take a little more time to tidy-up the lables, etc. first.

After noticing a layout problem with the 2x10 connector, I ended up re-routing the entire board from scratch.  This was OK because I could apply a lot of stuff I've learned and in the end it turned out much better than the last version.

Placed an order for 3 copies of the board today, now hopefully it actually works!


# 04232019

Boards came back faster than expected.  Not only that, they even work!  I documented the build process in detail in a blog post:

https://jjg.2soc.net/2018/04/23/beginners-luck-kicad-part-2/

As noted, something isn't quite right and I'm hoping I can just correct it in software, but I'm not sure when I'll have time to experiment with that.


# 04242018

Began testing software solutions to the panel driver (considering renaming this "panel interface") board.  Results are good, it appears that the electrical connections are sound (I can toggle each LED on and off) but the sequence is reversed from what I expected.  This can easily be compensated in the panel driver software so that's where it will be done, and I'll leave the board design alone for now.

I'm not exactly sure what to work on next.  There's a lot of possibilities but I want to put a little thought into it to make sure I pick the thing that will move the project the furthest along in light of both the current status of the work, and the resources I have avaliable.


# 05072018

Had some good conversations about how I might get a RISC-V core into the system.  Sounds like I might be able to use Sifive's design even if I can't get my hands on their chips.  Here's what I *think* I need to do:

* Download the HiFive bitstream
* Select an FPGA big enough for it
* "burn" that bitstream to the FPGA
* Make a SOPINE-compatible carrier board for the FPGA


# 05192018

Resumed work on the front-end interface board (I wish I had a good name for this).  It's essentially an extension of the Clusterboard panel interface connected via the 40-pin "PiBus" connector and adds the ability to switch power to the Clusterboard as well as monitor power consumption for the entire machine.  

Power switching is simply a matter of driving a relay using a transition via one of the GPIO pins on the MCP23017.  For power measurement I'm planning to use an INA219 i2c current monitor.  This IC is surface-mount and as such is available on several break-out boards, but I'm tempted to try and include the raw chip in my design even if it means dealing with a little SMD soldering.

Unfortunately Kicad doesn't have a symbol for this so I'm back to either tracking one down or making one.  This shouldn't be a big deal but I'm not good enough at it to avoid loosing gumption when I bump into it.

Additionally, I realized that I should probably prototype this design on a breadboard first (even though it's mostly based on existing work).  I'd hate to trip-over some limitation with the selected components/implementation only designing & ordering boards.  Maybe I should order one of those break-out boards and at mock-up at least the power switching/measuring side of things before committing them to a board?


# 05212018

Arduino announced an FPGA product over last weekend, so I'm not the only one who thinks there is demand for making FPGA development more accessible.

I also put the hardware from Mark I up for sale.  Since the HAD Prize money didn't come through I'm going to have to find other ways to fund work on Mark II.

All that got me thinking again about the goals of Mark II and refocused me on the proof-of-concept nature of the machine: to create an ARM-based alternative to the Intel-based Mark I and compare performance.  I don't need to complete the custom electronics I've been designing in order to complete this phase of the project so I'm adjusting priorities again.

With this in mind I think the next step should be to procure at least 5 more SOPINE modules (for a total of six), matching the node count of Mark I.  This will allow me to run hpl in the same configuration on both systems and answer the question of what the performance delta is between the Intel and ARM based systems.


# 05222018

The next step for RAIN Mark II will be completing the hardware build sufficiently to run hpl and establish the relative performance between Mark I and Mark II.  To this end the following hardware will need to be procured:

Mark II hardware complete BOM

| qty | price | desc | notes |
| --- | ----- | ---- | ----- |
| 5 | $29.00 | SOPINE | |
| 6 | $0.50 | SOPINE heatsink | | 
| 1 | | 5v Cooling fan | Large, slow, low-noise | 
| 1 | | Back panel | lasercut acrylic like the front | 
| 2 | $12.35 | Front-panel interface PCB (x3) | Front-panel interface for Clusterboard nodes |
| 1 | | Front-end interface PCB | Front-panel and power control interface for A64 node |
| 7 | | MCP23017 | i2c port multiplier for interface boards | 
| 2 | | 3.3 LDO | 3.3v for Clusterboard front-panel interfaces |
| ? | | ? | Header connectors for front-panel LED pigtails | 
| 1 | | Crimping tool | Crimp tool matching pigtail connectors | 
| 48 | | Resistors | |
| 6 | | Right-angle headers | | 
| 7 | | Straight headers | |
| 1 | | Relay | |
| 1 | | Transistor | | 
| 1 | | Current sensor | | 
| 1 | | MOSFET | Fan driver |


# 05232018

I think I need to more carefully delineate between what is needed to get the Mark II hardware to a state where I can complete the hpl testing and what actually *finishing* the build is like.

Technically the front-panel interfaces, the front-end power management, etc. are not needed to complete the machine to a state where I can perform the hpl tests.  It may be necessary to actively cool the machine, but that can be accomplished by other less-elegant means that completing the front-end interface and installing a software-controlled cooling fan in the back panel, etc.

So in terms of prioritization maybe I should be focused on acquiring only what is necessary to complete the hpl benchmarking and put-off the rest of the hardware until that is complete?


The bare minimum hardware necessary to complete a meaningful hpl benchmark of the Mark II machine is:

| qty | price | desc | notes |
| --- | ----- | ---- | ----- |
| 6 | $29.00 | SOPINE | https://www.pine64.org/?product=sopine-a64 |
| 6 | $8.56 | 16GB MicroSD Card | https://www.amazon.com/SanDisk-Ultra-Micro-Adapter-SDSQUNC-016G-GN6MA/dp/B010Q57SEE/ref=sr_1_3 |
| 8 | $0.50 | Heatsink | https://www.pine64.org/?product=rock64pine-a64-heatsink |

All-told it's about $240 worth of parts shipped.  

Unrelated, I'm looking into using [Ganglia](https://en.wikipedia.org/wiki/Ganglia_(software)).  At first I thought it was kind of overkill, but it was handy when I was working on Mark I and it doesn't seem like it will have significantly averse effects on performance (and may help improve the front-panel software).


# 06152018

Preparing for a big push this weekend.  I got my hands on six more SOPINE modules which will bring Mark II to full processing power.  This should be all that is *necessary* to complete Mark II's original mission of measuring performance difference between a traditional HPC cluster (Mark I) and an ARM-based machine of similar scale.

In addition to the SOPINE modules I have also procured enough components to build-out the two remaining front panel interface boards I received with my OSH Park order as well as the tools and parts to create non-hardwired connections to the front-panel LEDs, switches, etc.  I was on the fence about building more of these, because on one hand it would be cool to complete this part of the build, but on the other hand I have a couple key improvements I want to make to the FPI's so I'm hesitant to spend too much time and money on building more copies the current design.

So the first objective this weekend is to get the results of a hpl run.  The second objective is to build-out as much of the Mark II hardware as I can with the time and parts I have.  A potential third objective is to breadboard the front-end interface and/or re-design the FPI.

I've also offered to design an "attached processor" chassis for the Clusterboard for a friend on Mastodon, so that might make it into the mix.

Here's the plan for objective #1:

1. Burn Armbian image to SD cards
2. Install SOPINEs in the Clusterboard
3. Boot Clusterboard and try to ssh in to new boards
4. If ssh doesn't work, attach serial console to each board for initial configuration
5. Install linpack prerequisites on each node
6. Execute initial hpl run
7. Tune and re-run hpl until satisfied

It may be necessary to actively cool the machine during these hpl runs.  I won't take any steps to disable CPU scaling until I can find an affirmative way to measure CPU temp (the current Armbian build doesn't produce values in the `/sys` filesystem that I can translate).  If I have a suitable 5v fan on-hand I'll see about hacking-up one of the prototype front-panels to mount it on the back of the case.  If not I'll just use some other fan to blow air across the modules to try and improve performance.

## FEI interlude

The FEI (Front-End Interface) is responsible for the following:

* Switching Clusterboard power on and off
* Measuring power consumption via current sensor
* Managing active cooling
* Driving front-panel controls (LED's and switches) for the Front-End processor

My original plan was to build this around the same port expander used in the FPI's but I started reconsidering this due to the cost of the MCP23017 and the complexity of the host-based software to drive all the various functions.  As an alternative I considered using a microcontroller like the ATMEGA which could run some firmware to manage things like active cooling without bothering the A64 Front-End node.  I was on the fence about this but then a request from a friend to design a Clusterboard chassis w/o the FE clinched the usefulness of an MCU-based version of this interface.

What I have in mind is something similar to an Arduino (likely Arduino-compatible since that will make writing/modifying the firmware easy) with its GPIO connected to the various peripherals and a communication channel between the MCU and the A64.  I'm not sure what's the best channel, [i2c comes to mind](http://www.forkrobotics.com/2012/05/relay-control-over-i2c/) first (if only to be consistent with the other interface boards) but I'm not sure if that makes sense for this sort of communications.

The data that will be presented across the communications channel will include data from the A64 representing system state (network status, overtemp, cpu load, etc.).  The MCU will then sense the position of the status/load toggle switch and display the selection on the panel LED's via it's own GPIO.  I think this will make the front-panel interface more responsive (since it doesn't have to wait for the script to run like the python-based FPI's) and I *think* it will reduce load on the A64 as well since it will simply be pushing data across an already open channel.

Inputs:										Outputs:

mode switch ->								-> Clusterboard power relay
temp sensor ->								-> Fan MOSFET
current sensor ->
5vdc power ->								-> 5vdc power
											-> LED's (x5)

### Firmware

* Read temp sensor
* Set fan speed
* Add temp sensor reading to outbound data package
* Read current sensor
* Add current sensor reading to outbound data package
* Read inbound host date
* Read mode switch
* Update LED display
* Write outbound data to host


# 06172018

Note: this needs to be merged into the main build_log.md but I didn't have the most recent version of that checked-in when I started work on a different machine this morning.

Today begins the build to complete the first objective of Mark II.  When complete I'll have the results of an hpl run across 8 nodes to compare with the hpl results from Mark I.

Step 1 is to prepare the operating system for the new nodes.  This will be Armbian Bionic (nightly mainline kernel master branch 4.17.y).

I'm using my Pinebook for this (in an effort to eat my own dogfood) and an SD card inserted into the built-in slot shows up as /dev/mmcblk1.  Here's the command to burn the image to the card:

    sudo dd if=Armbian_5.46.180611_Pine64so_Ubuntu_bionic_dev_4.17.0.img of=/dev/mmcblk1 bs=4M

I hoped this image would boot and DHCP an address so I could configure it via ssh, but that didn't happen.  I swapped-out the SD card with one from my original SOPINE and it booted but I couldn't get a serial console.  After about an hour of troubleshooting I was able to connect via serial, so I'm writing all the details down here so I don't have to go through that again:

SOPINE Clusterboard serial terminal console connection:

White -> Pin 7
Green -> Pin 8
Black -> Pin 9

Command: screen /dev/ttyUSB0 115200

With a working serial console it was easy to see that the new image doesn't work.  Trying the Armbian Xenial image (Armbian_5.38_Pine64so_Ubuntu_xenial_default_3.10.107.img) next...

That image didn't work either so I'm falling-back to the custom one I used before (Armbian_5.41_Pine64so_Ubuntu_xenial_next_4.14.33.img) from this thread on the Pine64 forum: https://forum.pine64.org/showthread.php?tid=5738

...still getting "no ethernet found" at boot...

Since the new board boots correctly with the old SD card, I'm going to try and clone that card.  One caveot is that the MAC address is generated at first boot apparently, so I'll need to remove the "ethaddr" line from /boot/uEnv.txt to generate a new one for each board as described here: https://www.armbian.com/pine64/


# 06172018

Gave up on using the Pinebook and switched to the battleship.  Re-downloaded the build from the Pine64 forum and burned it to a new card with Etcher.  This booted and worked without issue so I'm not sure yet what the source of the problem, but I'll start determining that by re-burning the original card using etcher to rule-out a bad card.

Card works fine so it must have been a problem burning the cards.  This could be something Etcher fixed or possibly something bus-related with the Pinebook.

To get armbian-config to run without complaining the node needs Internet access.  I've set this up before by using the front-end as a router, but for some reason that config doesn't stick.  This might be OK (since it's more secure to keep them limited to the local network) but for now this temporary change gives them access:

## Config frontend as router

```
sudo iptables -A FORWARD -i eth0 -j ACCEPT
sudo iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
```

This worked well to get all the nodes setup, however I ran into network problems with one of the SOPINE modules.  This appears to be a hardware problem because swapping-out the SD card and the Clusterboard slot have no effect.

For now I'm just going to leave it as-is and keep it in the slot with the serial console attached.  

## DHCP Leases 

```
lease 192.168.1.13 {
  starts 0 2018/06/17 15:02:28;
  ends 0 2018/06/17 15:12:28;
  cltt 0 2018/06/17 15:02:28;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 02:ba:07:f5:65:33;
  client-hostname "node-4";
}
lease 192.168.1.16 {
  starts 0 2018/06/17 15:02:30;
  ends 0 2018/06/17 15:12:30;
  cltt 0 2018/06/17 15:02:30;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 02:ba:60:61:86:fe;
  client-hostname "node-0";
}
lease 192.168.1.14 {
  starts 0 2018/06/17 15:02:30;
  ends 0 2018/06/17 15:12:30;
  cltt 0 2018/06/17 15:02:30;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 02:ba:04:1e:38:a3;
  client-hostname "node-3";
}
lease 192.168.1.12 {
  starts 0 2018/06/17 15:02:33;
  ends 0 2018/06/17 15:12:33;
  cltt 0 2018/06/17 15:02:33;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 02:ba:c3:af:3d:10;
  client-hostname "node-5";
}
lease 192.168.1.15 {
  starts 0 2018/06/17 15:02:34;
  ends 0 2018/06/17 15:12:34;
  cltt 0 2018/06/17 15:02:34;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 02:ba:cf:05:0d:71;
  client-hostname "node-2";
}
lease 192.168.1.10 {
  starts 0 2018/06/17 15:02:34;
  ends 0 2018/06/17 15:12:34;
  cltt 0 2018/06/17 15:02:34;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 02:ba:fd:75:04:47;
  client-hostname "node-6";
}
lease 192.168.1.11 {
  starts 0 2018/06/17 15:02:38;
  ends 0 2018/06/17 15:12:38;
  cltt 0 2018/06/17 15:02:38;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 02:ba:2c:8b:62:89;
  client-hostname "node-1";
}

```

Reviewing that list I now see that all 7 nodes are on the network.  I'm not sure what went wrong with node-0 before, but it's working now.


# 06182018

I didn't make as much progress as I wanted over the weekend (in fact it was a major disappointment) but I was able to get the hardware and operating system configuration of Mark II to a point where it's possible to perform some hpl runs.  I won't be able to run full-speed until additional cooling is provisioned but I should be able to get all of the software configuration in place and perform some initial tests.

Before installing hpl though I want to be able to monitor the cluster using something less awkward than sshing into every node.  I've been happy with Ganglia in the past so I'm going to give that another shot.

Ganglia links:  

* https://www.ibm.com/developerworks/library/l-ganglia-nagios-1/index.html
* https://www.digitalocean.com/community/tutorials/introduction-to-ganglia-on-ubuntu-14-04

Since Ganglia is already configured on the FE node I should just need to install & configure the "agent" on each processing node.

To install agent:

sudo apt-get install -y ganglia-monitor

Edit /etc/ganglia/gmond.conf and set cluster.name to "RAIN Mark II".  
Restart the service with `sudo service ganglia-monitor restart` and wait for data to flow in. 

This works reasonably well.  Hostnames are not displayed, hosts are intermittently reported as "down" and the details about each node are missing/incorrect but it's probably good enough for now.  I'd like this to work better, but I'm not sure how much time I want to invest in becoming a Ganglia expert (at least not until I get some hpl results).

I spent more time on this than I wanted to.

I did find some reasonable documentation for digging-in further:

https://github.com/ganglia/monitor-core/blob/master/ganglia.pod

I'm going to limit myself to "before lunch" and see if I can get this into a stable configuration.

After pointing the FE node to listen on for multicast on the eth0 interface (`mcast_if = eth0` in the `udp_send_channel` section of `gmond.conf`) and making sure the cluster name & owner matched across all nodes (and numerous service restarts) all 32 cores showed up in the web UI!

I think this is good enough to move on to hpl testing.  I'd like the hostnames to show up properly and I'd love to get things like temperature in the graphs, but none of that is necissary to begin working on performance testing.

## Steps to running hpl

* ~~Get ssh keys on all nodes~~
* Install hpl prerequisites (necessary?)
	- MPI
* Copy hpl software to each node
	- on FE: ``scp -r ./openmpi 192.168.1.10:/home/jjg`
	- on node-x:
		+ ``cd ./openmpi/buildDir/bin`
		+ ./mpirun
		+ libnuma error
		+ `sudo apt-get install libnuma-dev`
		+ ./mpirun
		+ "nothing to do error" (this means it's working)
	+ on FE: `scp -r ./hpl 192.168.1.10:/home/jjg`
	+ on node-x: 
	+ on FE:
		`cd /home/jjg/hpl/bin/aarch64`
		`/home/jjg/openmpi/buildDir/bin/mpirun -nolocal -np 2 -machinefile /home/jjg/machines /home/jjg/hpl/bin/aarch64/xhpl`
		got dependency error
	+ on node-x:
		* `cd /home/jjg/hpl/bin/aarch64/`
		* `./xhpl`
		* got dependency error
	+ on FE:
		* `scp -r ./ATLAS/ 192.168.1.10:/home/jjg`
	+ on node-x:
		* ./xhlp
		* got dependency error but also noticed HPL config error (need at least 4 procs...)
	+ on FE:
		* `/home/jjg/openmpi/buildDir/bin/mpirun -nolocal -np 4 -machinefile /home/jjg/machines /home/jjg/hpl/bin/aarch64/xhpl`
		* hpl runs!
* Run
* Tune
* Run again

I need to dig-out my documentation to see what command lines have worked in the past.  I also remember running into all sorts of weird problems so hopefully I noted their solutions somewhere...

After some fiddling I was able to get hpl running with 4 processes on one node, here's the output:

```
================================================================================
HPLinpack 2.2  --  High-Performance Linpack benchmark  --   February 24, 2016
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   10000 
NB     :      80 
PMAP   : Row-major process mapping
P      :       2 
Q      :       2 
PFACT  :    Left 
NBMIN  :       2 
NDIV   :       2 
RFACT  :    Left 
BCAST  :   Blong 
DEPTH  :       0 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR04L2L2       10000    80     2     2             268.44              2.484e+00
HPL_pdgesv() start time Mon Jun 18 17:53:55 2018

HPL_pdgesv() end time   Mon Jun 18 17:58:23 2018

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0017047 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
```

Now I just need to repeat the *working* steps above for each node:

1.  on FE: `scp -r ./openmpi 192.168.1.10:/home/jjg`
2.  on FE: `scp -r ./hpl 192.168.1.10:/home/jjg`
3.  on each node: `sudo apt-get install -y libnuma-dev`
4.  on FE: add IP's of each node to `/home/jjg/machines


Once the software has been installed on all nodes benchmark can be run by issuing the following on the front-end node (from in the `/home/jjg/hpl/bin/aarch64` directory):

`/home/jjg/openmpi/buildDir/bin/mpirun -nolocal -np 4 -machinefile /home/jjg/machines /home/jjg/hpl/bin/aarch64/xhpl`

The next step is to crank-up the process count so we put all the cores in the cluster to work.  I'm a bit hesitant to do this w/o active cooling but what's the worst that could happen?

First let's just crank-up the `-np` argument:

`/home/jjg/openmpi/buildDir/bin/mpirun -nolocal -np 28 -machinefile /home/jjg/machines /home/jjg/hpl/bin/aarch64/xhpl`

No significant difference was measured by simply increasing the `-np` value.  A change to the hpl.conf is probably needed as well.

For this run I changed the `p` and `q` values in HPL.dat to 7 and 4 respectively.  I don't know if this is a *good* arrangement but I *think* it will create a problem that can be spread-out across the cluster.

Still not clear that this is working as expected.  Using the HPL.dat found here as a guide: https://www.howtoforge.com/tutorial/hpl-high-performance-linpack-benchmark-raspberry-pi/ another test was run with the same start command as above.


Still the same results. Upon closer inspection of the output, it looks like the changes I'm making to HPL.dat are being ignored.  I was wondering if it would be necessary to copy the config to all nodes and as it turns out, doing so makes the changes take effect. Long-term this is an awkward solution but it will do for now.

Output:

```
================================================================================
HPLinpack 2.2  --  High-Performance Linpack benchmark  --   February 24, 2016
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   17400 
NB     :     128 
PMAP   : Row-major process mapping
P      :       4 
Q      :       6 
PFACT  :   Right 
NBMIN  :       4 
NDIV   :       2 
RFACT  :   Crout 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR11C2R4       17400   128     4     6             272.05              1.291e+01
HPL_pdgesv() start time Mon Jun 18 19:13:02 2018

HPL_pdgesv() end time   Mon Jun 18 19:17:34 2018

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0011654 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
```

This is definitely working more of the cluster now.  The performance results are disappointing, but there's plenty of tuning to be done (also temp may be a problem already) so I'm not sweating that now.

I found some old notes from Mark I including the HPL.dat file from the best hpl run (9.5 Gflops).  I'm trying another run with these settings just to see what happens.  After that I'm going to hold-off until I can get some way to see if the CPU's are being throttled (due to temp or other reasons) before spending too much more time trying to tune the config.


## The best-performing Mark I HPL.dat:

```
HPLinpack benchmark input file
Innovative Computing Laboratory, University of Tennessee
HPL.out      output file name (if any)
6            device out (6=stdout,7=stderr,file)
1            # of problems sizes (N)
20000         Ns
1            # of NBs
128           NBs
0            PMAP process mapping (0=Row-,1=Column-major)
1            # of process grids (P x Q)
4            Ps
4            Qs
16.0         threshold
1            # of panel fact
2            PFACTs (0=left, 1=Crout, 2=Right)
1            # of recursive stopping criterium
4            NBMINs (>= 1)
1            # of panels in recursion
2            NDIVs
1            # of recursive panel fact.
1            RFACTs (0=left, 1=Crout, 2=Right)
1            # of broadcast
5            BCASTs (0=1rg,1=1rM,2=2rg,3=2rM,4=Lng,5=LnM)
1            # of lookahead depth
1            DEPTHs (>=0)
2            SWAP (0=bin-exch,1=long,2=mix)
64           swapping threshold
0            L1 in (0=transposed,1=no-transposed) form
0            U  in (0=transposed,1=no-transposed) form
1            Equilibration (0=no,1=yes)
8            memory alignment in double (> 0)
```


## Mark II results using the above config:

```
================================================================================
HPLinpack 2.2  --  High-Performance Linpack benchmark  --   February 24, 2016
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   20000 
NB     :     128 
PMAP   : Row-major process mapping
P      :       4 
Q      :       4 
PFACT  :   Right 
NBMIN  :       4 
NDIV   :       2 
RFACT  :   Crout 
BCAST  :  BlongM 
DEPTH  :       0 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR05C2R4       20000   128     4     4             560.01              9.525e+00
HPL_pdgesv() start time Mon Jun 18 19:48:26 2018

HPL_pdgesv() end time   Mon Jun 18 19:57:46 2018

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0008587 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
```


Let's take a closer look at this:

Mark I with this config:
`WR15C2R4       20000   128     4     4             567.27              9.403e+00`

Mark II with the same config:
`WR05C2R4       20000   128     4     4             560.01              9.525e+00`

I'm not sure exactly how to interpret this.  Either Mark II is already faster node-for-node than Mark I, or I did a bad job of measuring Mark I's performance.  In either case it will be interesting to re-run with the problem scaled-out to match Mark II's increased node count and see if we can break the 10 Gflop barrier.

Along the way I ran across an interesting paper that includes HPL testing of ARM-based SOC's and small clusters:

https://static.epcc.ed.ac.uk/dissertations/hpc-msc/2011-2012/Submission-1126390.pdf


I switched from a 4x4 (16 core) grid to 4x6 (24 cores) to see if that's enough to break the 10G barrier.  This is the highest safe grid we can do without using an oddball size.  I might be able to go as high as 5x6 (30 cores) if I can figure out why the FE node hangs anytime I involve it (likely a power supply issue).

Results:

```
================================================================================
HPLinpack 2.2  --  High-Performance Linpack benchmark  --   February 24, 2016
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   20000 
NB     :     128 
PMAP   : Row-major process mapping
P      :       4 
Q      :       6 
PFACT  :   Right 
NBMIN  :       4 
NDIV   :       2 
RFACT  :   Crout 
BCAST  :  BlongM 
DEPTH  :       0 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR05C2R4       20000   128     4     6             394.71              1.351e+01
HPL_pdgesv() start time Mon Jun 18 20:05:22 2018

HPL_pdgesv() end time   Mon Jun 18 20:11:57 2018

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0009369 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
```

Clearly there's more to this than incrementally increasing the grid size...


# 06202018

Doing some noodling on how to squeeze more hpl performance out of Mark II.  It's really mysterious to me that performance dropped-off when I *increased* the node count.  Maybe that's a function of problem size (i.e. too much overhead moving data around) or maybe it's the un-squareness of the P & Q configuration?  Just not sure.

Came across this tool for tuning HPL: http://hpl-calculator.sourceforge.net/hpl-calculations.php via this PDF: http://www.crc.nd.edu/~rich/CRC_Summer_Scholars_2014/HPL-HowTo.pdf

I tried the suggested settings for Mark II but unfortunately the results were still much lower than the 4x4 run earlier:

```
================================================================================
HPLinpack 2.2  --  High-Performance Linpack benchmark  --   February 24, 2016
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   38912 
NB     :     128 
PMAP   : Row-major process mapping
P      :       4 
Q      :       7 
PFACT  :   Right 
NBMIN  :       4 
NDIV   :       2 
RFACT  :   Crout 
BCAST  :  BlongM 
DEPTH  :       0 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR05C2R4       38912   128     4     7            2364.06              1.662e+01
HPL_pdgesv() start time Wed Jun 20 18:03:07 2018

HPL_pdgesv() end time   Wed Jun 20 18:42:32 2018

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0008191 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
```

Looks like there's more work to do before I'll be able to get the full capacity out of this machine.

# 06212018

Continuing work on finding the optimal HPL.dat for Mark II.  Running a number of experiments in "batch mode" with various settings derived from the tool linked above.  So far none of the results begin to approach my old settings in terms of peak performance, but I'm learning a lot about how they impact the machine by watching the results and the monitors.

Some interesting facts I've learned studying the HPL documentation above:

* The SOC in the SOPINE is ARM Cortex-A53 and as such can do 2 dual-precision instructions per cycle
* SOPINE clock speed is 1.2Ghz

Using the tool above I re-ran the 4x4 test using the suggested N value of 29440.  This resulted in a slightly higher score of 9.863 Gflops.  If I can find a way to work a few more cores into this I should be able to break 10...

Poking-around while waiting for tests to run I noticed that running lscpu on the cluster nodes doesn't show what clock speed they are running at.  There's a number of hardware-ish things like that that seem to be missing from the OS running on these nodes, I'm not sure why that is or what I can do about it, but it's going to become important once I start doing more fine-grained tuning.

Unrelated, I found this guide to creating i2c slaves using Arduino (which might come in handy for the FE interface since I'm planning to use an Atmega for that now):

http://dsscircuits.com/articles/arduino-i2c-slave-guide

All this new info makes me want to fire Mark I back up and see if I can squeeze a few more Gflops out of it.


# 06222018

More experiments with HPL.dat.

Interestingly reducing `N` gives a slight bump to 4x4 performance, almost enough to break 10 Gflops.  However adding another node drops the number back to just under 2.  I'm going to go back-to-basics and start with one node, adding more to see how things change.

| N | NB | P | Q | Gflops |
|---|----|---|---|--------|
| 13056 | 128 | 1 | 4 | 2.559 |
| 13056 | 128 | 2 | 4 | 4.811 |
| 13056 | 128 | 3 | 4 | 6.635 |
| 13056 | 128 | 4 | 4 | 8.580 |
| 13056 | 128 | 5 | 4 | 9.858 |
| 13056 | 128 | 6 | 4 | 1.154 |
| 13056 | 128 | 7 | 4 | 1.247 |
| 13056 | 200 | 7 | 4 | 1.210 |
| 13000 | 200 | 7 | 4 | 1.200 |
| 13056 | 96 | 7 | 4 | 1.323 |
| 34656 | 96 | 7 | 4 | 1.766 |
| 34656 | 96 | 4 | 7 | 1.823e+01 |

Oh wow, I might be reading these results very wrong :)  1.662e+01 doesn't mean 1.6 Gflops, it means 16.62.  This explains why my hpl performance appears to drop when additional nodes are added.

derp!

Let's try again now that we are smart enough to read numbers...


| N | NB | P | Q | Gflops | memfill |
|---|----|---|---|--------|---------|
| 34656 | 96 | 4 | 7 | 1.823e+01 | 80% |
| 38912 | 128 | 4 | 7 | 1.654e+01 | 90% |


# 06272018

Weird, looks like I'm missing an entry in the log.  I did some tests to figure out why I couldn't measure temp and CPU speed on the cluster nodes and found out that they are running a different kernel than the front-end node.  

* Front-end node: ARMBIAN 5.38 stable Ubuntu 16.04 LTS 3.10.107-pine64
* Cluster nodes: ARMBIAN 5.41 user-built Ubuntu 16.04.4 LTS 4.14.33-sunxi64

The cluster nodes are running the "mainline" kernel and the front-end is running the "legacy" kernel.  The mainline kernel has limitations, one of which is that it doesn't support CPU throttling when the chip gets hot, so the clock speed is locked at some lower-than-maximum speed ([408Mhz?](https://forum.armbian.com/topic/1917-armbian-running-on-pine64-and-other-a64h5-devices/?page=2&tab=comments#comment-15225)) for safety reasons.  This is probably part of the reason I can't get anywhere near the Rpeak of the machine.

From the Armbian website:

*"At the moment only basic functionality is implemented and no thermal protection (throttling) is working (no cpufreq scaling also and no access to PMIC too). Therefore also pretty conservative settings are used which negatively impact performance."*

Now that I know this (and I know how to read the hpl output correctly) I'm going to stop gunning for peak performance until I have time to learn how to get the best kernel installed and finish the hardware work to support dynamic active cooling and power management.  In the meantime I'm going to run a battery of tests to get an idea of how performance scales as nodes are added to the system.  This should give me an idea of how far the system can be scaled as-is and still yield performance improvements.

For this test I'm going to use a fixed block size of 96 (the block size used when the best results so far were obtained) and a memory fill of 80% and an assumed clock speed of 408Mhz.  This may not be ideal across all configurations but I don't have a clear idea of how the block size should change as node count goes up so I'm going to eliminate it as a variable.

| Nodes | N | NB | P | Q | Gflops (est) | Gflops (actual) |
|-------|---|----|---|---|--------------|-----------------|
| 1 | 13056 | 96 | 1 | 4 | 2 | 2.723e+00 |
| 2 | 18528 | 96 | 2 | 4 | 5 | 5.238e+00 |
| 3 | 22656 | 96 | 2 | 6 | 7 | 7.909e+00 |
| 4 | 26208 | 96 | 2 | 8 | 10 | 1.040e+01 |
| 5 | 29280 | 96 | 4 | 5 | 13 | 1.292e+01 |
| 6 | 32064 | 96 | 4 | 6 | 15 | 1.562e+01 |
| 7 | 34656 | 96 | 4 | 7 | 18 | 1.826e+01 |

*note: I tried to use the HPL.dat configuration options to run the tests above as a batch, but it didn't work the way I expected so I had to do 7 runs with 7 different configs.*

In addition to these tests I should do some network bandwidth tests using `iperf3` just to confirm that my tests are not being impacted by known network interface issues.

Out of curiosity, I took the best configuration above and tried it again with a 90% memory fill:

| Nodes | N | NB | P | Q | Gflops (est) | Gflops (actual) |
|-------|---|----|---|---|--------------|-----------------|
| 7 | 38976 | 96 | 4 | 7 | 18 | 1.851e+01 |

Incremental improvement...worth further experimentation.

| Nodes | N | NB | P | Q | Gflops (est) | Gflops (actual) |
|-------|---|----|---|---|--------------|-----------------|
| 7 | 38912 | 128 | 4 | 7 | 18 | 1.665e+01 |
| 7 | 38976 | 96 | 4 | 7 | 18 | 1.851e+01 |


# 06282018

Reading an article about [hosting your git repositories on your own server](https://hackaday.com/2018/06/27/keep-it-close-a-private-git-server-crash-course/) and the thought crossed my mind: could git be used to distribute software across the nodes of a cluster?

i.e., could you configure each node as a `remote`, and then `git push` updated code and have it distributed automatically?

I talk a lot about finishing the front-end interface but what exactly does that mean, and when am I actually going to get around to doing it?

The FEI is needed to do the following:

* Interface the front-panel controls (LED's, switches) with the front-end node
* Switch power to the Clusterboard
* Monitor power consumption
* Sense internal cabinet temperature
* Drive cooling fan(s)

There are other things the interface could do, but these are the essential parts.

I've decided to use a microcontroller (MCU) for this (as opposed to the i2c port expander used in the other interfaces) for a number of reasons, the primary being that it allows the FEI to be more "autonomous" and reduce the amount of extra overhead placed on the front-end node.  This also allows the FEI to be used without the front-end node to support an "attached processor" version of the chassis.

So what's it going to take to actually get this *done*?

I think the first step is to breadboard a prototype.  I've done a few sketches of the schematic and ran into a few hang-ups because I'm not sure how some of the parts should be spec'd, etc.  What I need to do is get a list of potential components, stick them on a breadboard and experiment until I have a configuration that works.  Nice thing about this is that I haven't selected any components yet that *require* PCB mounting so I should be able to make a working prototype on a breadboard that I can actually connect to Mark II and test it as if it were the "real thing".

The other thing that sort of mandates building a working prototype is that I'll need to write some "firmware" for the MCU.  This probably means frequently re-programming the MCU so having something more accessible (and physically reconfigurable) than the finished board is advantageous.

So what parts do we need to start prototyping?

| qty needed | qty on-hand | cost | name | description | notes |
|------------|-------------|------|------|-------------|-------|
| 1 | 1 | ? | Atmega 328 | MCU | I picked this because I'm familiar with it, and it's inexpensive |
| 1 | 0 | ? | 5vdc relay | 5vdc coil, 5vdc @ 15A or better contacts | Should be PCB-mountable |
| 1 | 1? | ? | Temperature sensor | Analog or digital should do | I think I have an analog one |
| 1 | 1? | ? | Current sensor | Analog or digital, 5vdc @ 15A or better | I think I have an analog one |
| 1 | 1? | ? | MOSFET | 5vdc or better | To drive the fan |
| 1 | 0 | ? | 5vdc fan(s) | Cooling fan | May need more than one |
| ? | ? | ? | Resistors | For front-panel interface | |
| ? | ? | ? | Headers | For front-panel interfaces | |
| ? | ? | ? | MCU support components | Misc. bits needed for MCU | Crystal, caps, etc.? |
| ? | ? | ? | A64 connector | Connects to pins on the "Pi Bus" | |
| ? | ? | ? | Transistor, etc. | To connect relay to MCU | This might require other components |

This is going to take some dedicated time (I don't think it's going to get done by just "chipping-away" at it).  If I can set-aside a half-day I think I can answer the unanswered questions above and fill-out the missing bits of data in that table.  Then I can order the necessary parts and schedule another half-to-full day of work to assemble a working prototype.

Once I have a working prototype I can translate that into a PCB design and get some boards ordered.

So the "blocking function" is setting-aside a few hours to figure out what's missing from the table above.  Time to take a look at the calendar...

Unrelated, some FPGA articles for later:

* https://hackaday.com/2018/06/12/real-world-pipelining-for-fpgas/
* https://hackaday.com/2018/06/05/pipelining-digital-logic-in-fpgas/

... and HPC / hpl / etc. links:

* https://static.epcc.ed.ac.uk/dissertations/hpc-msc/2011-2012/Submission-1126390.pdf

KiCad part database: https://kcdb.ciphersink.net/

Also this: http://www.mikedrivendevelopment.com/2018/06/what-i-learned-making-my-own-jit.html


# 07162018

Ran across a paper using FPGA's to accelerate rendering Mandlebrot sets.  I might be able to take some ideas from the design for use in "programmable logic" modules for RAIN:

http://people.ece.cornell.edu/land/courses/ece5760/FinalProjects/s2018/mch258_mtr73_jsp263/final_report_mch258_mtr73_jsp263/final_report_mch258_mtr73_jsp263/index.html


# 08012018

For fun I'm going to run some traditional grid computing loads on Mark II just to see what happens.

Maybe not, realized that managing the nodes on the other side of a NAT'd connection might be more time-consuming than I can invest at the moment.


# 08082018

Broke the build on one of the SOPINE modules trying to get a kernel with temp/etc. support working so I'm going to try something new.

This time, I'm downloading the ["standard" SOPINE image](http://files.pine64.org/os/SOPINE/ubuntu/xubuntu-xenial-mate-20170306-longsleep-sopine-8GB.img.gz) just to see what happens when I stick that in the Clusterboard.

Well, it boots, and networking seems to work (although looks like the front-end didn't remember my iptables config, grr...); now to see if it can tell it's temp and run at full-speed...

Fastest way to see if we can get what we want might be to install the Ganglia agent and see what shows-up in the monitor?  If that doesn't work we can try something harder, but if it *does*, then we might know everything we need to know.

...damn unattended updates are running so I can't install anything until that's done.  Definately something I'll want disabled if I end up going with this distribution...

Also noticed that I'm missing one of the nodes, not sure if I smoked a second one during the kernel experiments the other day...

...ok this is exciting!

After about an hour of updating I was able to install the Ganglia agent and with a small config change the node showed-up in the console within minutes. Not only that, it has the correct CPU speed!

The next step is to see if I can get what I need to run HPL installed and see how it performs under load.  If that goes well I'll probably rebuild the rest of the cluster using this image.

## Steps to setup new Ubuntu-based node:

1. new node: `sudo apt-get install ganglia-monitor`
2. new node: Set cluster name in `/etc/ganglia/gmond.conf`(?)
3. new node: `sudo systemctl restart ganglia-monitor`
4. new node: `sudo adduser jjg`
5. fe: `ssh-copy-id jjg@192.168.2.17`
6. fe: `scp -r ./openmpi 192.168.2.17:/home/jjg`
7. fe: scp -r ./hpl 192.168.2.17:/home/jjg
8. new node: `sudo apt-get install -y libnuma-dev`

Before HPL can be run we'll need to update the `machines` file as well as the `hpl_go.sh` batch file to reflect any new/changed host IP addresses.

Modified the HPL.dat, machine file and script to run on a single node and dispatched it to the new build.  Not only did it work, but it turned in the best single-node performance I've had with this machine (3.058 Gflops!).

Based on this I think it makes sense to start rebuilding the cluster.  I know there's things that this image has which are not needed/wasteful for this application but given the performance improvement I think it makes sense to move forward.

Just for fun I cranked-up the config to run on all nodes now avaliable to see if the new node moves the needle running across the cluster compared to historical data.  Watching the logs as this runs, it's clear that thermal-initiated throttling is kicking-in.  This is encouraging because it means that active cooling may increase performance (which wasn't the case using the previous OS build).

Results of this test were a little disappointing, slightly below historical (15.58 vs 15.62 Gflops) but I'm hopeful that with some thermal management that number can be increased.


# 08092018

After copying the new image to the SD cards the system didn't boot as expected.  Turns out all nodes ended up with the same MAC address.

Using the serial console I reset the MAC's one at a time by deleting the `ethaddr` line  from `/boot/uEnv.txt`.

I think this means that the MAC was hard-coded into the image I burned to the SD cards, but I haven't confirmed that.  I'll have to look into that later.

Now to re-install the basic tools for monitoring and HPL.  .22 has a lock on APT so it must be auto updating.  We'll have to circle-back and set that node up later.

I also installed the copper heat-sinks I picked-up for the cluster nodes while I was fooling-around with re-flashing the SD cards.  It will be interesting to see to what degree these alone impact single-node performancei (I also think they will make a difference when we get to active cooling as well).

With all nodes except .22 setup, we can re-run the 6-node test from yesterday and see what impact the new kernel & heatsinks have on performance.

Results were slightly better this time, 16.11 Gflops.  Now let's repeat with the fan on...

When I went to the lab to setup the fan the machine was noticably hot, I could feel heat radiating up from the Clusterboard.  I setup the fan in a not-very-scientific way and re-ran the 6-node test, just like above.  

I immediately noticed a difference in the logs.  Without the fan you could see thermal management kicking-in and throttling the CPU's.  With the fan on I' not seeing that on the few nodes I've sampled (all with a system load of 4+).  This really surprises me, I expected to see overheating reduced, but I didn't expect it to be eliminated.

Active cooling matters: 18.94 Gflops.

Now to retest the whole cluster with this setup.  Results:

```
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   34656 
NB     :      96 
PMAP   : Row-major process mapping
P      :       4 
Q      :       7 
PFACT  :   Right 
NBMIN  :       4 
NDIV   :       2 
RFACT  :   Crout 
BCAST  :  BlongM 
DEPTH  :       0 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0


================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR05C2R4       34656    96     4     7            1254.39              2.212e+01
HPL_pdgesv() start time Thu Aug  9 21:18:17 2018

HPL_pdgesv() end time   Thu Aug  9 21:39:11 2018

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0006911 ...... PASSED
================================================================================
```


# 08102018

Yesterdays results were very encouraging.  Not only did Mark II break the 20 Gflops barrier, but it did so with zero optimization of the standard SOPINE OS image.  This means that there's likely more performance to be had by slimming-down the vast array of thing installed and running by default on that image.

It also confirms that the Armbian image previously used was limiting the performance of the cluster nodes (not that this was a surprise, but it's good to have the theory confirmed).  It's also encouraging to see that with a very crude active cooling setup the nodes can be run at full-speed without CPU throttling kicking-in.

I think a good next-step will be to tune the build on an individual node to come up with a "recipe" of what changes can be made to maximize performance of the build.  This could then be applied across the cluster.  Ideally, I could make an image of this tuned build and re-initialize the SD cards of the other cluster nodes with this image, as well as back-up a copy for future use.  I'm not sure if it's simply a matter of using `dd` to make an image of the SD card or if something more complex is required, but I guess we'll cross that bridge when we get there.

Regardless the first step is to run a couple tests against a single node to determine the current baseline for performance.  For this I'll run three tests and take the average.

| Nodes | N | NB | P | Q | Gflops (est) | Run 1 | Run 2 | Run 3 | Avg Gflops |
|-------|---|----|---|---|--------------|-------|-------|-------|------------|
| 1 | 13056 | 96 | 1 | 4 | 7 | 3.395 | 3.407 | 3.408 | 3.403 |

With baseline performance recorded for the "stock" SOPINE image, we can start removing unnecessary software and see what gains can be had.  The simplest way I can think of to start this process is to use the `top` command while the system is idling and start uninstalling in their order of CPU consumption (assuming they are not components we need to keep of course).

`top` didn't really give me what I wanted to I used `apt list --installed` to see what was installed and started picking-off things I knew we didn't need, starting with x-windows:

`apt-get purge libx11.* libqt.*`

Running a quick test after removing the components above the result is 3.378 Gflops, essentially no improvement.  Worth noting that I didn't reboot the node before this test (due to the hang-on-reboot bug) so there may be some x components resident in memory.

Just for fun, I kicked the memory numbers up to 90% (`N 14688`) to see if that made a difference in the single-node configuration.  The idea is that after uninstalling things there may be more memory to work with.

The results of 90% fill was 3.500 Gflops, a decent improvement.  While this ran I watched closely at the system memory usage and 90% is probably as far as we can go safely (92% might be possible but that's pushing it).

Let's purge everything we can, reboot and re-test.

```
sudo apt-get purge -y ubuntu-mate*
sudo apt-get purge -y xfonts*
sudo apt-get purge -y x11*
sudo apt-get purge -y cups*
sudo apt-get purge -y fonts*
sudo apt-get purge -y gnome*
sudo apt-get purge -y gstreamer*
sudo apt-get purge -y samba*
sudo apt-get purge -y modemmanager*
sudo apt-get purge -y mate*
sudo apt-get purge -y lightdm*
sudo apt autoremove
```

...there's probably more that can go but there's *so much* to go through, these seem like the biggest wins.

Re-ran 90% fill after reboot and only got 21.33 Gflops, re-running with the earlier settings.

Still 21.34, not sure what changed but clearly more work to do to get consistent results.



# 08152018

It's time to wrap-up Mark II.

There's a lot of cool stuff I could be running on Mark II and now that I have an O/S build that supports the CPU's full clock speed, it's probably time to stop obsessing over benchmarks and get things buttoned-up enough to demonstrate the machine outside of the lab.

I know I've said this a dozen times before but each time I get hung-up exploring some new avenue.  I think these have all been worthwhile, but I think if I can finalize the Mark II build I can start working on demonstrations of more practical workloads that might help turn others on to the project.

So what needs to be done before Mark II can (safely) leave the lab?  I think it falls into four categories:

## Front Panel

The current front-panel interface (FPI) boards have issues but are probably "good enough" to finish Mark II.  I need to do some work to make them fit safely without interfering with the heatsinks, and I need to see how much clearance there is with the headers attached (as opposed to hard-wired LED connections) but I think I can make it work.  I'll also need to order 4 more boards (which means ordering six) as well as the port expander chips.  The existing panel should be good enough.

## Power

There's a number of good reasons to build a front-end interface (FEI) to manage power, cooling, etc. but designing and building it is going to require a lot of prototyping and most likely a couple board designs.  There's also a need for firmware development and the associated tooling, so it's no small task and waiting on that has contributed significantly to delaying the completion of Mark II.  Now that the system has been running more regularly I'm more confident that a staged power-up (waiting for the front-end to be ready before applying power to the Clusterboard) is less critical than before.  There's numerous reasons to continue pursuing this work, and it will be a key part of Mark III designs, but if pressed I can probably work-around the need for this to expedite the completion of Mark II.

## Cooling

As mentioned above the original intention was to have cooling managed by the FEI, however there may be a reasonable way to control fan speed using GPIO on the front-end, and have the cluster nodes supply the front-end with temperature data from their internal sensors.  If not this, then the "dumb" approach of just running the fan full-speed anytime the power is on is an option, and all that's required is some wiring and a back panel with a suitable hole cut in it...

## Mounting

Finally both the Clusterboard and the A64 need to be mounted in a more permanent fashion to the inside of the chassis.  This might be possible using screws and standard mounting hardware (bent metal, etc.) or might require a little 3D printing.  With the requirement to make servicing the hardware easy off the table (the Mark IIb/Mark III chassis solves this) there's no need for fancy mounting that makes the boards easily removable, etc.

## Problems with this approach

We won't be able to measure power consumption, and might not be able to regulate fan speed.  There's no way to power-off/reset an individual cluster node, which coupled with the "broken reboot" issue makes resetting single nodes time-consuming and disruptive.  Beyond that I'm not sure there's anything worth noting that blocks this from meeting the objective of the Mark II machine.  There's a number of annoyances, and missing features, but they are all things that probably should wait until Mark III anyway.


## Good enough?

If the above can be executed as expected, I should be able to demonstrate the machine publicly, and turn my focus toward the software side of things for awhile.  If nothing else it will put a clear line between Mark II and Mark III which is probably a good idea lest Mark II feature-creep its way along for another year...



# 08262018

When I saw the USB sockets on the Clusterboard the thought crossed my mind that there might be a less labor-intensive, user-approachable alternative to the front-panel interface boards and discrete LED's: would it make more sense to create a long circuit board that contained the front-panel LEDs, switches, etc. that plugged directly into the USB port and avoided using the header pins?

Today I found out that Adafruit sells [Neopixel "sticks"](https://www.adafruit.com/product/1426) that sound a lot like what I had in mind.  These do not interface directly to USB, but I did a little research and it appears to be fairly simple to create an interface board containing a small MCU that can drive the Neopixels and provide a serial-based interface (like any other Arduino-type device) to the host O/S.  

This doesn't address the front-panel input controls (switches) but this "bridge" board could be designed to include them, or they could be mounted directly to the panel in the traditional way.  The power/reset switch will still need to be connected to the SOPINE module's header pins, so *some* wiring outside of these boards will be required to preserve currently-desined functionalty.

I'd like to experiment with this, because it dramatically reduces the complexity of assembling the system and eliminates the need for special tools (to crimp the LED connectors, for example).

The more I think about it, the switches should probably be connected directly to the panel. If not, they shouldn't be fastened to it otherwise removing the panel will require removing all the switch fasteners or unplugging all the LED "strips" from their USB sockets. That might be viable in a design where the front-panel was installed by sliding it down toward the board, but the current design has the panel removed from the front which wouldn't work with the USB connectors as they are.

So here's how I imagine this going together:

Cluserboard USB socket -> bridge board (attiny running Arduino firmware) -> Neopixel Stick

The bridge board could include terminals for the "mode" switch read by the firmware to toggle between "load" and "status" modes.  The host O/S would feed all this data to the attiny via serial and the position of the switch would simply determine which data was displayed on the LED's.  This means toggling between the two could be very fast without having to constantly poll.

I'm not sure what MCU is ideal for this, but something that can do serial over USB w/o additional hardware would be helpful.  The [Digispark](http://digistump.com/wiki/digispark) does most of what I think I need so something with a simular design might be the ticket.


# 08312018

Came up with a couple ideas for packaging in the new "b" chassis so I'm going to spend some time today trying to design some new printables.

# 09052018

Some interest on Mastodon around pscp, might need to sink some more cycles into that...

@Phreaker@mastodon.technology
@dirtycommo@anticapitalist.party


# 09102018

Had a bit of a breakthrough with the portable design this weekend.  After talking to a few people about their interest in it, I realized that while it's unlikely I could create the most powerful ARM supercomputer (given my resources), it's might be possible to create the most powerful ARM *laptop*.  With this inspiration I took another look at the PSCP design and started laying things out more seriously.

I was able to find a layout that I think will work and fits within the bounding box of an A4-sized sheet.  The choice of A4 isn't completely arbitrary, I had an A4-sized sheet of acrylic left-over from the original Mark II front panel work.  The idea is that if I can get the machine to fit within that size, I can easily cut the large flat parts out of acrylic on the laser I have access to, and I can make the other parts using 3D printing (I'd 3d print the whole thing, but my printers suck at printing large flat/thin parts).  With this in mind I laid-out the electronics of Mark II on the acrylic sheet until I found an arrangement I liked, and it came together surpringly well!

There's a few more parts I'll need for the portable (namely a keyboard, display and battery), but for now I'll mock those up in paper/carboard to work-out the fit and hold-off on ordering the real thing until I have something that works.

In the meantime I'm going to work on designing the 3d printed parts and potentially start bolting thigns to the acrylic so the whole thing can be moved around as a fixed unit.

Right now I'm planning to use the LCD that Pine64 sells on their website since the specs are sufficient, the price is reasonable and it works with the A64's built-in interface.  This means no HDMI adapters, etc. and increases the likelyhood that I'll be able to make it work.  This display also includes a touchscreen, which is what I've been leaning towards in place of a trackpad, etc.  Personally I'll probably use a mouse, but it's important to have *some* method of pointer input that is self-contained in the machine.

For the keyboard I'm using a custom design built out of Cherry-MX switches.  I might prototype this as 3d printed parts but I imagine the later versions will be lasercut, ideally aluminum but acrlyc should work if that's the only option.  Right now I assume this will use a USB interface but it would be better if I can use some of the A64's GPIO instead and leave the two USB ports avaliable for use outside the machine.

That leaves the battery, which I haven't decided on yet, but it's probably going to require some custom electronics to manage charging (the A64 has a lipoly input but no charging circuitry that I'm aware of).  This board can also do power management (measure power consumption, etc.) as well as active thermal management (because we now know some fans will be required to run at maximum speed).

Some design references:

* https://www.reddit.com/r/Nerf/comments/2wzl5j/this_is_the_ucm21_as_seen_in_robocop_2_a_real/
* http://richardsapperdesign.com/products/1980-1990/mod-5140
* https://duckduckgo.com/?q=tandy+1400&t=canonical&iar=images&ia=images


# 01182019

It's been awhile.

Today I bolted the clusterboard and the front-end node to an A4-sized piece of 3mm acrylic.  You see I have an application for the machine, and I don't want to wait until the chassis is complete to have a safe(r) way to work with it.

This layout is based on the portable work described earler.  I don't have any intention to go down that complete road, but we'll see what happens.  I designed a printable mount for a 40mm fan to add some active cooling to this setup.  It's not in use yet (I need to order more fans) but I wanted to design the parts so I could make sure there was room for them after I mounted the boards.

The application I'm using the cluster for is [Apache Spark]().  This may or may not be a great fit, but it's something I'm working with now and if I can make RAIN run it well enough, it might be a way to get more people excited about the project.

## Setting up Spark

1. sudo apt-get update
2. sudo apt-get install default-jdk
3. Add `$JAVA_HOME="/usr/lib/jvm/java-8-openjdk-arm64"` to `/etc/environment`
4. `source /etc/environment`
5. `wget http://apache.cs.utah.edu/spark/spark-2.4.0/spark-2.4.0-bin-hadoop2.7.tgz` (or `scp` from front-end host)
6. `tar zxf spark-2.4.0-bin-hadoop2.7.tgz`
7. `cd `
8. Start the master: `./sbin/start-master.sh`
8. Start a slave: `./sbin/start-slave.sh spark://<master host/ip>:7077`


# 01222019

Did some more work on the 40mm fan mounts.  Switched to a modular design since my (working) 3D printer can't print a mount large enough for 4 fans in one pass.

I'm pretty happy with the results.  I need to decide how to power the fans now, and it's likely that they will have to run at a fixed-speed since I don't want to delay things coming up with speed control electronics right now.  I'm going to review the Clusterboard schematics and see if I can find a 12vdc connection on the board with enough current to drive the four fans, maybe add a switch (since they're not needed unless the system is working at fullspeed).

Turns out there's no 12vdc on the Clusterboard (this is obvious when you realize that the power supply is only 5vdc... duh).  I should have paid attention to this when I ordered the fans.  In any event this is easy to fix by picking-up some 5v fans, but it will delay things a few days.

Looks like we can get 5v straight off the power jack using the unpopulated four-pin power connector spot "J33".  It's tempting to just hardware something to those holes but it's probably more prudent to put a connector in there.  I guess we'll see how impatient I get.

The ATX connector will work as well, so I could pick-up an ATX female connector and use that (in fact I probably have one I can recycle somewhere in the lab...?)

# 07222019

Been awhile...

Decided to switch-gears and focus on building the portable. I think this is the direction with the most room for innovation on this platform at the moment.

I've re-designed the 3D printed parts of the case starting with a basic building block.  This will let me get the basics nailed before moving on to the most complex bits like the keyboard.

Speaking of keyboards, I've decided to go with full-sized keys in an "ortholinear" layout.  This "should" fit within the size limits (still shooting for an A4 base) and let me use pre-fab keycaps.

I'm going to have to put this project on hold for the next few weeks so I don't expect to have much more of an update other than this, but I wanted to note the keyboard info, and make sure I remembered to upload the new parts models to the repository.


## References
* https://docs.qmk.fm/#/newbs
* https://thomasbaart.nl/category/mechanical-keyboards/firmware/qmk/qmk-basics/
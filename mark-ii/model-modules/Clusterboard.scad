module Clusterboard(){

  BOARD_WIDTH = 176;
  BOARD_DEPTH = 170;
  BOARD_HEIGHT = 3;
  MOUNTING_HOLE_RADIUS = 2.92 / 2;
  MOUNTING_HOLE_OFFSET = MOUNTING_HOLE_RADIUS;

  difference(){
    // board
    cube([BOARD_WIDTH,BOARD_DEPTH,BOARD_HEIGHT]);
    
    // mounting holes
    translate([3.65+MOUNTING_HOLE_OFFSET,3.65+MOUNTING_HOLE_OFFSET,-1]){
      cylinder(r=MOUNTING_HOLE_RADIUS, 6);
    }
    
    translate([3.65+MOUNTING_HOLE_OFFSET,BOARD_DEPTH - (8.86+MOUNTING_HOLE_OFFSET),-1]){
      cylinder(r=MOUNTING_HOLE_RADIUS, 6);
    }
    
    translate([BOARD_WIDTH - (3.65+MOUNTING_HOLE_OFFSET),BOARD_DEPTH - (31.5+MOUNTING_HOLE_OFFSET),-1]){
      cylinder(r=MOUNTING_HOLE_RADIUS, 6);
    }
    
    translate([BOARD_WIDTH - (3.65+MOUNTING_HOLE_OFFSET),3.65+MOUNTING_HOLE_OFFSET,-1]){
      cylinder(r=MOUNTING_HOLE_RADIUS, 6);
    }
  }
  
  // modules
  translate([20,15,3]){
    cube([135,85,30]);
  }
  
  // atx power
  translate([176-9.7,83,3]){
    cube([9.7,51.5,13]);
  }
}

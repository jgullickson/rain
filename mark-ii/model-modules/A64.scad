module A64(){

  BOARD_WIDTH = 127;
  BOARD_DEPTH = 80;
  BOARD_HEIGHT = 3;
  MOUNTING_HOLE_RADIUS = 3.35 / 2;
  MOUNTING_HOLE_OFFSET = MOUNTING_HOLE_RADIUS;
  
  difference(){
  
    // board
    cube([BOARD_WIDTH, BOARD_DEPTH, BOARD_HEIGHT]);
    
    // mounting holes
    translate([2.7+MOUNTING_HOLE_OFFSET, 2.6+MOUNTING_HOLE_OFFSET, -1]){
      cylinder(r=MOUNTING_HOLE_RADIUS, h=6);
    }
    
    // mounting holes
    translate([2.6+MOUNTING_HOLE_OFFSET, BOARD_DEPTH - (2.6+MOUNTING_HOLE_OFFSET), -1]){
      cylinder(r=MOUNTING_HOLE_RADIUS, h=6);
    }
    
    // mounting holes
    translate([BOARD_WIDTH - (2.6+MOUNTING_HOLE_OFFSET), BOARD_DEPTH - (3.0+MOUNTING_HOLE_OFFSET), -1]){
      cylinder(r=MOUNTING_HOLE_RADIUS, h=6);
    }
    
    // mounting holes
    translate([BOARD_WIDTH - (2.6+MOUNTING_HOLE_OFFSET), 3.0+MOUNTING_HOLE_OFFSET, -1]){
      cylinder(r=MOUNTING_HOLE_RADIUS, h=6);
    }
  }

}

# RAIN Mark II

## Summary

Mark II machines are currently based on [PINE64](http://pine64.org) ARM-based single-board computers.  Work is currently focused on the classic microcomputer-inspired Supercomputer Trainer (SCT) but some design work has begun on a portable version as well.

## RAIN Mark II Supercomputer Trainer (sct)

### Status

On-hold.  Current work is focused on the portable.

### Description

[RAIN-SCT](sct) is a 64-bit ARM-based personal supercomputer [trainer](https://en.wikipedia.org/wiki/List_of_home_computers#List_of_hobby,_kit,_or_trainer_computers).  Based on the [PINE64 Clusterboard](https://www.pine64.org/?product=clusterboard-with-7-module-slots-include-one-free-sopine-module-during-promotion-period), RAIN-PSC is designed to accommodate 8 nodes (7 Clusterboard-hosted [SOPINE modules](https://www.pine64.org/?page_id=1491) and 1 [PINE A64 LTS](https://www.pine64.org/?product=pine-a64-lts) head node) for a total of 32 ARM cores, 16 vector/GPU cores and 16GB of memory.

RAIN-PSC's design is inspired by early personal computers of the 1970's ([ALTAIR 8080](https://en.wikipedia.org/wiki/Altair_8800), [IMSAI 8080](https://en.wikipedia.org/wiki/IMSAI_8080), etc.) and features a practical and beautiful array of front-panel controls.

### Notes

Command line to run hpl:

`/home/jjg/openmpi/buildDir/bin/mpirun -np 8 -machinefile /home/jjg/machines /home/jjg/hpl/bin/aarch64/xhpl`


## RAIN Mark II Personal Supercomputer Portable (pscp)

### Status

In-progress.  Iterating on assis design and implementation.

### Description 

The [pscp](pscp) is a design concept for a portable version of RAIN-PSC.  Inspired by early portable personal computers such as the [GRiD Compass](https://en.wikipedia.org/wiki/Grid_Compass) and (original) [Macintosh Portable](https://en.wikipedia.org/wiki/Macintosh_Portable), RAIN-PSCP packs the capabilities of RAIN-PSC into a portable-but-rugged stand-alone package perfect for developing high-performance application on-site or in a cabin deep in the woods.

# Raiden

an open-source personal supercomputer

I'm designing an open-source personal supercomputer to make high-performance computing applications more accessible to programmers and users alike.

As long as I can remember, the hardware side of high-performance computing has outstripped the software development side. Writing software that makes the most of the vast computing resources provided by supercomputers has been a perannial challenge. This was less of a problem in the last few decades as the number of supercomputers was low enough, and their cost high enough that a small number of specialized programmers was enough to meet the demand for high-performance computing applications.

However during the last decade or so the cost of supercomputer construction has dropped making these systems avaliable beyond the traditional military, university and high-end corporate research environments. This in turn has allowed high-performance computing to be applied to a wider range of problems and the result is an increasing number of applications which demand supercomputer-class systems.

While supercomputers have become less expensive they still require resources beyond the grasp of most organizations and almost any individual software developer. We expected "the cloud" to address this gap (and to some degree it has), however there's an array of reasons that make applying cloud-style scalability to high-performance applications. These reasons include performance, predictability and security, among others.

For developers the cloud provides additional challenges. While it's possible to assemble a high-performance computing-style cluster using cloud resources, constructing and maintaining a system like this is non-trivial even for experienced administrators. It's also very hard to know *what* to construct without existing experience with high-performance systems. On top of this the cost structure of cloud providers are often so complicated it's very hard to understand what building and operating a system like this will cost. If a programmer manages to get through all this and develop a piece of software, it's very hard to understand how much it will cost to run it "at scale" and decide whether or not the application is sufficiently optimized (or even worth it).

Raiden addresses these issues by providing a "turnkey" supercomputer that a programmer can purchase and use as much or as little as they like for a fixed price. Raiden strikes a balance between providing an environment which matches a parallel supercomputer while remaining small and inexpensive enough for an individual to own and operate. Beyond the hardware, Raiden provides an operating system and software stack allowing programmers to focus on programming without having to learn the art of building & maintaining a high-performance cluster.

Of course this "sandbox" would be of limited use if there wasn't a way to turn the software developed on it loose on production-sized workloads. For this Raiden offers several advantages over the alternatives.

First, Raiden's developer tools can leverage their understanding of the hardware to analyze a workload and project what resources are needed to scale-up the workload. For example, if a programmer writes a simulation program that can execute the simulation of 100 objects in one hour on a single Raiden computer, Raiden's developer tools can estimate how much more power is needed to simulate 1000 objects in the same amount of time, or 100 objects in half the time, etc. This lets programmers optimize their code for projected workload (and available resources) without unnecessary over-optimization.

Raiden is designed to be a modular, scalable architecture so it's possible for a programmer to scale-up a single Raiden computer to meet the application's projected needs or transfer the job to a larger system. Alternatively Raiden provides another option to run big jobs: the global grid.

Each Raiden computer ships with the ability to participate in a grid of Raiden computers connected via the public Internet. Operators who enable this feature trade unused processing resources for grid credit. This credit can then be used to purchase surplus computing power from other operators when needed to run large jobs. This allows Raiden to provide the elasticity of the cloud without dependence on a single vendor or proprietary system.

how much more power is needed to simulate 1000 objects in the same amount of time, or 100 objects in half the time, etc. This lets programmers optimize their code for projected workload (and available resources) without unnecessary over-optimization.

Raiden is designed to be a modular, scalable architecture so it's possible for a programmer to scale-up a single Raiden computer to meet the application's projected needs or transfer the job to a larger system. Alternatively Raiden provides another option to run big jobs: the global grid.

Each Raiden computer ships with the ability to participate in a grid of Raiden computers connected via the public Internet. Operators who enable this feature trade unused processing resources for grid credit. This credit can then be used to purchase surplus computing power from other operators when needed to run large jobs. This allows Raiden to provide the elasticity of the cloud without dependence on a single vendor or proprietary system.

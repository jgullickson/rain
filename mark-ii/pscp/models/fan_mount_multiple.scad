$fn=50;

MOUNT_LENGTH = 61;
MOUNT_DEPTH = 15;
MOUNT_HEIGHT = 3;

// try to stick to one fastener
MOUNTING_HOLE_DIAMETER = 3.5;

difference(){

  union(){
/*
    // left end
    color("red"){
      translate([-7,0,0]){
        base(MOUNT_LENGTH+7);
      }
    }

    // middle
    color("green"){
      translate([61,0,0]){
        base(40);
      }
    }
*/
    // right end
    color("blue"){
      translate([101,0,0]){
        base(70);
      }
    }
  
  }
  // overlapping ends (optional)
  // left 
  /*
  translate([-1,-1,-1]){
    cube([9,MOUNT_DEPTH+2,MOUNT_HEIGHT]);
  }
  */
  
  // right
  /*
  translate([MOUNT_LENGTH-8,-1,2]){
    #cube([9,MOUNT_DEPTH+2,MOUNT_HEIGHT]);
  } 
  */

  // fan(s)
  translate([0,2,0]){
    for (i=[0:3]){
      translate([i*41,0,3]){
      //translate([(MOUNT_LENGTH/2) - (40/2),0,5]){
        #fan();
      }
    }
  }
}

module base(length){
  difference(){
    union(){
    
      // base
      cube([length,MOUNT_DEPTH,MOUNT_HEIGHT]);

      // flange
      translate([0,12,MOUNT_HEIGHT]){
        cube([length,MOUNT_DEPTH - 12,10]);
      }
    }
    // base mounting holes
    translate([4,(MOUNT_DEPTH/2)-5,-1]){
      #cylinder(r=MOUNTING_HOLE_DIAMETER/2, h=MOUNT_HEIGHT+2);
    }
  
    translate([length-4,(MOUNT_DEPTH/2)-5,-1]){
      #cylinder(r=MOUNTING_HOLE_DIAMETER/2, h=MOUNT_HEIGHT+2);
    }
  }
}

module fan(){

  FAN_HOLE_DIAMETER = 38;
  HOLE_DEPTH = 25;
  
  //difference(){
  union(){
  
    // bounding box
    cube([40,10.3,40]);

    // blade opening
    translate([20,-1,20]){
      rotate([-90,0,0]){
        cylinder(r=FAN_HOLE_DIAMETER/2,h=HOLE_DEPTH);
      }
    }
    
    // mounting holes
    DISTANCE_FROM_BOUNDS = (2.27 + (3.5/2));
    
    translate([DISTANCE_FROM_BOUNDS,-1,DISTANCE_FROM_BOUNDS]){
      rotate([-90,0,0]){
        cylinder(r=MOUNTING_HOLE_DIAMETER/2,h=HOLE_DEPTH);
      }
    }

    translate([DISTANCE_FROM_BOUNDS,-1, 40 - DISTANCE_FROM_BOUNDS]){
      rotate([-90,0,0]){
        cylinder(r=MOUNTING_HOLE_DIAMETER/2,h=HOLE_DEPTH);
      }
    }
    
    translate([40 - DISTANCE_FROM_BOUNDS,-1, 40 - DISTANCE_FROM_BOUNDS]){
      rotate([-90,0,0]){
        cylinder(r=MOUNTING_HOLE_DIAMETER/2,h=HOLE_DEPTH);
      }
    }
    
    translate([40 - DISTANCE_FROM_BOUNDS,-1,DISTANCE_FROM_BOUNDS]){
      rotate([-90,0,0]){
        cylinder(r=MOUNTING_HOLE_DIAMETER/2,h=HOLE_DEPTH);
      }
    }  
  }
}

$fn=50;

MOUNT_LENGTH = 60;
MOUNT_DEPTH = 15;
MOUNT_HEIGHT = 5;

// try to stick to one fastener
MOUNTING_HOLE_DIAMETER = 3.5;

difference(){
  union(){
  
    // base
    cube([MOUNT_LENGTH,MOUNT_DEPTH,MOUNT_HEIGHT]);

    // flange
    translate([0,11,MOUNT_HEIGHT]){
      cube([MOUNT_LENGTH,MOUNT_DEPTH - 11,10]);
    }
  }

  // base mounting holes
  translate([5,(MOUNT_DEPTH/2)-3,-1]){
    #cylinder(r=MOUNTING_HOLE_DIAMETER/2, h=MOUNT_HEIGHT+2);
  }
  
  translate([MOUNT_LENGTH-5,(MOUNT_DEPTH/2)-3,-1]){
    #cylinder(r=MOUNTING_HOLE_DIAMETER/2, h=MOUNT_HEIGHT+2);
  }
  
  translate([10,0,0]){
    for (i=[0:0]){
      translate([i*50,0,5]){
      //translate([(MOUNT_LENGTH/2) - (40/2),0,5]){
        #fan();
      }
    }
  }
}


module fan(){

  FAN_HOLE_DIAMETER = 38;
  HOLE_DEPTH = 25;
  
  //difference(){
  union(){
  
    // bounding box
    cube([40,10.3,40]);

    // blade opening
    translate([20,-1,20]){
      rotate([-90,0,0]){
        cylinder(r=FAN_HOLE_DIAMETER/2,h=HOLE_DEPTH);
      }
    }
    
    // mounting holes
    DISTANCE_FROM_BOUNDS = (2.27 + (3.5/2));
    
    translate([DISTANCE_FROM_BOUNDS,-1,DISTANCE_FROM_BOUNDS]){
      rotate([-90,0,0]){
        cylinder(r=MOUNTING_HOLE_DIAMETER/2,h=HOLE_DEPTH);
      }
    }

    translate([DISTANCE_FROM_BOUNDS,-1, 40 - DISTANCE_FROM_BOUNDS]){
      rotate([-90,0,0]){
        cylinder(r=MOUNTING_HOLE_DIAMETER/2,h=HOLE_DEPTH);
      }
    }
    
    translate([40 - DISTANCE_FROM_BOUNDS,-1, 40 - DISTANCE_FROM_BOUNDS]){
      rotate([-90,0,0]){
        cylinder(r=MOUNTING_HOLE_DIAMETER/2,h=HOLE_DEPTH);
      }
    }
    
    translate([40 - DISTANCE_FROM_BOUNDS,-1,DISTANCE_FROM_BOUNDS]){
      rotate([-90,0,0]){
        cylinder(r=MOUNTING_HOLE_DIAMETER/2,h=HOLE_DEPTH);
      }
    }  
  }
}

// misc notes
// keyboard cad builder: http://builder.swillkb.com/
// keyboard models: https://www.thingiverse.com/thing:1848627/
// display: https://www.pine64.org/?product=7-lcd-touch-screen-panel

include <../../model-modules/Clusterboard.scad>
include <../../model-modules/A64.scad>

// 3mm A4 panel
color("lightgray")
cube([210,297,3]);

// Clusterboard
translate([190,290,3]){
  color("lightgreen")
  rotate([0,0,180]){
    Clusterboard();
  }
}

// A64 front-end node
translate([75,25,3]){
  color("lightgreen")
  A64();
}

// keyboard
translate([-10,10,15]){
  rotate([5,0,0]){
    color("lightblue")
    translate([0,0,0]){
        import("planck_topplate_part_a.stl", convexity=3);
    }
    color("lightblue")
    translate([79,0,0]){
        import("planck_topplate_part_b.stl", convexity=3);
    }
    color("lightblue")
    translate([156,0,0]){
        import("planck_topplate_part_c.stl", convexity=3);
    }
  }
}

// lcd panel
translate([20,150,50]){
  color("lightsalmon")
  rotate([70,0,0]){
    cube([163.9, 97.1, 2.6]);
  }
}

EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:relays
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "RAIN Mark II Front-End Panel and Power Controller"
Date "2018-04-16"
Rev "1.0.0"
Comp "Gullickson Laboratories"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_02X20 P?
U 1 1 5AD4E218
P 3350 3550
F 0 "P?" H 3350 4600 50  0000 C CNN
F 1 "CONN_02X20" V 3350 3550 50  0000 C CNN
F 2 "" H 3350 2600 50  0000 C CNN
F 3 "" H 3350 2600 50  0000 C CNN
	1    3350 3550
	1    0    0    -1  
$EndComp
$EndSCHEMATC

EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MCP23017
LIBS:panel_driver-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "RAIN Mark II Panel Interface"
Date "2018-04-07"
Rev "1.1.0"
Comp "Gullickson Laboratories"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCP23017 U1
U 1 1 5AC916EB
P 5650 3200
F 0 "U1" H 5100 4300 50  0000 C CNN
F 1 "MCP23017" H 6100 2100 50  0000 C CNN
F 2 "Housings_DIP:DIP-28_W7.62mm" H 5150 2100 50  0001 C CNN
F 3 "DOCUMENTATION" H 5650 3150 50  0001 C CNN
	1    5650 3200
	1    0    0    -1  
$EndComp
NoConn ~ 6600 2900
NoConn ~ 6600 4100
NoConn ~ 6600 4000
NoConn ~ 6600 2800
NoConn ~ 6600 3150
NoConn ~ 6600 3250
NoConn ~ 6600 3350
NoConn ~ 6600 3450
NoConn ~ 6600 3550
NoConn ~ 6600 3650
NoConn ~ 6600 3750
NoConn ~ 6600 3850
$Comp
L R R1
U 1 1 5AC91C76
P 8200 2000
F 0 "R1" V 8280 2000 50  0000 C CNN
F 1 "200" V 8200 2000 50  0000 C CNN
F 2 "Discret:R3" V 8130 2000 50  0001 C CNN
F 3 "" H 8200 2000 50  0000 C CNN
	1    8200 2000
	0    1    1    0   
$EndComp
$Comp
L R R2
U 1 1 5AC91CCC
P 8200 2200
F 0 "R2" V 8280 2200 50  0000 C CNN
F 1 "200" V 8200 2200 50  0000 C CNN
F 2 "Discret:R3" V 8130 2200 50  0001 C CNN
F 3 "" H 8200 2200 50  0000 C CNN
	1    8200 2200
	0    1    1    0   
$EndComp
$Comp
L R R3
U 1 1 5AC91CF4
P 8200 2400
F 0 "R3" V 8280 2400 50  0000 C CNN
F 1 "200" V 8200 2400 50  0000 C CNN
F 2 "Discret:R3" V 8130 2400 50  0001 C CNN
F 3 "" H 8200 2400 50  0000 C CNN
	1    8200 2400
	0    1    1    0   
$EndComp
$Comp
L R R4
U 1 1 5AC91D1F
P 8200 2600
F 0 "R4" V 8280 2600 50  0000 C CNN
F 1 "200" V 8200 2600 50  0000 C CNN
F 2 "Discret:R3" V 8130 2600 50  0001 C CNN
F 3 "" H 8200 2600 50  0000 C CNN
	1    8200 2600
	0    1    1    0   
$EndComp
$Comp
L R R5
U 1 1 5AC91D4D
P 8200 2800
F 0 "R5" V 8280 2800 50  0000 C CNN
F 1 "200" V 8200 2800 50  0000 C CNN
F 2 "Discret:R3" V 8130 2800 50  0001 C CNN
F 3 "" H 8200 2800 50  0000 C CNN
	1    8200 2800
	0    1    1    0   
$EndComp
$Comp
L GND #PWR01
U 1 1 5AC92057
P 7100 3650
F 0 "#PWR01" H 7100 3400 50  0001 C CNN
F 1 "GND" H 7100 3500 50  0000 C CNN
F 2 "" H 7100 3650 50  0000 C CNN
F 3 "" H 7100 3650 50  0000 C CNN
	1    7100 3650
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 5AC92236
P 7100 3100
F 0 "R6" V 7180 3100 50  0000 C CNN
F 1 "10k" V 7100 3100 50  0000 C CNN
F 2 "Discret:R3" V 7030 3100 50  0001 C CNN
F 3 "" H 7100 3100 50  0000 C CNN
	1    7100 3100
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR02
U 1 1 5AC92324
P 8100 3800
F 0 "#PWR02" H 8100 3650 50  0001 C CNN
F 1 "VCC" H 8100 3950 50  0000 C CNN
F 2 "" H 8100 3800 50  0000 C CNN
F 3 "" H 8100 3800 50  0000 C CNN
	1    8100 3800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5AC923D5
P 4500 3200
F 0 "#PWR03" H 4500 2950 50  0001 C CNN
F 1 "GND" H 4500 3050 50  0000 C CNN
F 2 "" H 4500 3200 50  0000 C CNN
F 3 "" H 4500 3200 50  0000 C CNN
	1    4500 3200
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR04
U 1 1 5AC92403
P 5650 1450
F 0 "#PWR04" H 5650 1300 50  0001 C CNN
F 1 "VCC" H 5650 1600 50  0000 C CNN
F 2 "" H 5650 1450 50  0000 C CNN
F 3 "" H 5650 1450 50  0000 C CNN
	1    5650 1450
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X10 P1
U 1 1 5AC932C6
P 3600 3250
F 0 "P1" H 3600 3800 50  0000 C CNN
F 1 "CONN_02X10" V 3600 3250 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_2x10" H 3600 2050 50  0001 C CNN
F 3 "" H 3600 2050 50  0000 C CNN
	1    3600 3250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 5AC936DF
P 5650 4850
F 0 "#PWR05" H 5650 4600 50  0001 C CNN
F 1 "GND" H 5650 4700 50  0000 C CNN
F 2 "" H 5650 4850 50  0000 C CNN
F 3 "" H 5650 4850 50  0000 C CNN
	1    5650 4850
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR06
U 1 1 5AC93C32
P 2900 2800
F 0 "#PWR06" H 2900 2650 50  0001 C CNN
F 1 "VCC" H 2900 2950 50  0000 C CNN
F 2 "" H 2900 2800 50  0000 C CNN
F 3 "" H 2900 2800 50  0000 C CNN
	1    2900 2800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 5AC93D57
P 3100 3200
F 0 "#PWR07" H 3100 2950 50  0001 C CNN
F 1 "GND" H 3100 3050 50  0000 C CNN
F 2 "" H 3100 3200 50  0000 C CNN
F 3 "" H 3100 3200 50  0000 C CNN
	1    3100 3200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 5AC93D93
P 4100 3200
F 0 "#PWR08" H 4100 2950 50  0001 C CNN
F 1 "GND" H 4100 3050 50  0000 C CNN
F 2 "" H 4100 3200 50  0000 C CNN
F 3 "" H 4100 3200 50  0000 C CNN
	1    4100 3200
	1    0    0    -1  
$EndComp
Text Label 4000 2450 0    60   ~ 0
RESET
Text Label 3150 2450 0    60   ~ 0
PWR_ON
NoConn ~ 3850 3600
NoConn ~ 3850 3500
NoConn ~ 3850 3400
NoConn ~ 3850 3300
NoConn ~ 3850 3200
NoConn ~ 3850 3100
NoConn ~ 3350 3100
NoConn ~ 3350 2900
NoConn ~ 3350 3300
NoConn ~ 3350 3400
NoConn ~ 3350 3500
$Comp
L PWR_FLAG #FLG09
U 1 1 5AC94F65
P 2600 2800
F 0 "#FLG09" H 2600 2895 50  0001 C CNN
F 1 "PWR_FLAG" H 2600 2980 50  0000 C CNN
F 2 "" H 2600 2800 50  0000 C CNN
F 3 "" H 2600 2800 50  0000 C CNN
	1    2600 2800
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG010
U 1 1 5AC94FA1
P 4300 3000
F 0 "#FLG010" H 4300 3095 50  0001 C CNN
F 1 "PWR_FLAG" H 4300 3180 50  0000 C CNN
F 2 "" H 4300 3000 50  0000 C CNN
F 3 "" H 4300 3000 50  0000 C CNN
	1    4300 3000
	1    0    0    -1  
$EndComp
NoConn ~ 3850 2800
$Comp
L CONN_01X16 P2
U 1 1 5ACA6596
P 9300 2650
F 0 "P2" H 9300 3500 50  0000 C CNN
F 1 "CONN_01X16" V 9400 2650 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x16" H 9300 2650 50  0001 C CNN
F 3 "" H 9300 2650 50  0000 C CNN
	1    9300 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 2300 6600 1900
Wire Wire Line
	6600 1900 9100 1900
Wire Wire Line
	6600 2400 6650 2400
Wire Wire Line
	6650 2400 6650 2100
Wire Wire Line
	6650 2100 9100 2100
Wire Wire Line
	6600 2500 6700 2500
Wire Wire Line
	6700 2500 6700 2300
Wire Wire Line
	6700 2300 9100 2300
Wire Wire Line
	6600 2600 6750 2600
Wire Wire Line
	6750 2600 6750 2500
Wire Wire Line
	6750 2500 9100 2500
Wire Wire Line
	6600 2700 9100 2700
Wire Wire Line
	5650 1450 5650 1850
Wire Wire Line
	5650 4550 5650 4850
Wire Wire Line
	5650 1650 4700 1650
Wire Wire Line
	4700 1650 4700 2300
Connection ~ 5650 1650
Wire Wire Line
	3850 3700 4700 3700
Wire Wire Line
	3350 3700 3350 3800
Wire Wire Line
	3350 3800 4700 3800
Wire Wire Line
	2600 2800 3350 2800
Wire Wire Line
	3350 3600 2900 3600
Wire Wire Line
	2900 3600 2900 2800
Wire Wire Line
	3100 3200 3350 3200
Wire Wire Line
	4700 2750 4500 2750
Wire Wire Line
	4500 2750 4500 3200
Wire Wire Line
	4700 2850 4500 2850
Connection ~ 4500 2850
Wire Wire Line
	4700 2950 4500 2950
Connection ~ 4500 2950
Wire Wire Line
	3350 3000 3150 3000
Wire Wire Line
	3150 3000 3150 2250
Wire Wire Line
	3850 3000 4300 3000
Wire Wire Line
	4100 3000 4100 3200
Wire Wire Line
	3850 2900 4000 2900
Wire Wire Line
	4000 2900 4000 2250
Connection ~ 2900 2800
Connection ~ 4100 3000
Wire Wire Line
	6750 2900 9100 2900
Wire Wire Line
	8200 3000 8200 3800
Wire Wire Line
	8200 3000 9100 3000
Wire Wire Line
	8200 3800 8100 3800
Wire Wire Line
	7100 2900 7100 2950
Connection ~ 7100 2900
Wire Wire Line
	7100 3250 7100 3650
Text Label 8450 1900 0    60   ~ 0
LED_1_POS
Wire Wire Line
	8350 2000 9100 2000
Text Label 8450 2000 0    60   ~ 0
LED_1_NEG
Text Label 8450 2900 0    60   ~ 0
MODE
Wire Wire Line
	8350 2200 9100 2200
Wire Wire Line
	8350 2400 9100 2400
Wire Wire Line
	8350 2600 9100 2600
Text Label 8450 2100 0    60   ~ 0
LED_2_POS
Text Label 8450 2300 0    60   ~ 0
LED_3_POS
Text Label 8450 2500 0    60   ~ 0
LED_4_POS
Text Label 8450 2700 0    60   ~ 0
LED_5_POS
Wire Wire Line
	8350 2800 9100 2800
Text Label 8450 2200 0    60   ~ 0
LED_2_NEG
Text Label 8450 2400 0    60   ~ 0
LED_3_NEG
Text Label 8450 2600 0    60   ~ 0
LED_4_NEG
Text Label 8450 2800 0    60   ~ 0
LED_5_NEG
Wire Wire Line
	9100 3100 8350 3100
Text Label 8450 3100 0    60   ~ 0
PWR_ON
Wire Wire Line
	9100 3200 8200 3200
Connection ~ 8200 3200
Wire Wire Line
	9100 3300 8350 3300
Text Label 8450 3300 0    60   ~ 0
RESET
Wire Wire Line
	7100 3400 9100 3400
Connection ~ 7100 3400
Wire Wire Line
	8050 2000 7600 2000
Wire Wire Line
	7600 2000 7600 3400
Connection ~ 7600 3400
Wire Wire Line
	8050 2200 7600 2200
Connection ~ 7600 2200
Wire Wire Line
	8050 2400 7600 2400
Connection ~ 7600 2400
Wire Wire Line
	8050 2600 7600 2600
Connection ~ 7600 2600
Wire Wire Line
	8050 2800 7600 2800
Connection ~ 7600 2800
Wire Wire Line
	6600 3000 6750 3000
Wire Wire Line
	6750 3000 6750 2900
$Comp
L AP1117 U2
U 1 1 5ACE57D8
P 3850 4600
F 0 "U2" H 3950 4350 50  0000 C CNN
F 1 "AP1117" H 3850 4850 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x03" H 3850 4600 50  0001 C CNN
F 3 "" H 3850 4600 50  0000 C CNN
	1    3850 4600
	1    0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 5ACE5801
P 4300 4050
F 0 "R7" V 4380 4050 50  0000 C CNN
F 1 "200" V 4300 4050 50  0000 C CNN
F 2 "Discret:R3" V 4230 4050 50  0001 C CNN
F 3 "" H 4300 4050 50  0000 C CNN
	1    4300 4050
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 5ACE5839
P 4550 4050
F 0 "R8" V 4630 4050 50  0000 C CNN
F 1 "200" V 4550 4050 50  0000 C CNN
F 2 "Discret:R3" V 4480 4050 50  0001 C CNN
F 3 "" H 4550 4050 50  0000 C CNN
	1    4550 4050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 5ACE58A0
P 3850 5100
F 0 "#PWR011" H 3850 4850 50  0001 C CNN
F 1 "GND" H 3850 4950 50  0000 C CNN
F 2 "" H 3850 5100 50  0000 C CNN
F 3 "" H 3850 5100 50  0000 C CNN
	1    3850 5100
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR012
U 1 1 5ACE58CC
P 3300 4600
F 0 "#PWR012" H 3300 4450 50  0001 C CNN
F 1 "VCC" H 3300 4750 50  0000 C CNN
F 2 "" H 3300 4600 50  0000 C CNN
F 3 "" H 3300 4600 50  0000 C CNN
	1    3300 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 4600 3550 4600
Wire Wire Line
	3850 4900 3850 5100
Wire Wire Line
	4150 4600 4550 4600
Wire Wire Line
	4550 4600 4550 4200
Wire Wire Line
	4300 4200 4300 4600
Connection ~ 4300 4600
Wire Wire Line
	4300 3900 4300 3800
Connection ~ 4300 3800
Wire Wire Line
	4550 3900 4550 3700
Connection ~ 4550 3700
$EndSCHEMATC

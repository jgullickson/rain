// PINE A64 mounting bracket

$fn=50;

VERSION = "v1.3";
CLEARANCE = 1;

// magnets
MAGNET_DIAMETER = 10;
MAGNET_DEPTH = 3;

// m3
SCREW_SHAFT_DIAMETER = 2.9;
SCREW_HEAD_DIAMETER = 5.40;
SCREW_HEAD_DEPTH = 3.5;
NUT_DIAMETER = 6.1;
NUT_DEPTH = 2.3;

BASE_HEIGHT= 10;
BASE_LENGTH = 35;
BASE_WIDTH = 10;

// BRACKET_LENGTH = 10;
BACK_HEIGHT = 10;
BACK_WIDTH = 3;

difference(){
    
    union(){
        
        // base
        cube([BASE_LENGTH,BASE_WIDTH,BASE_HEIGHT]);
        
        // back
        cube([BASE_LENGTH,BACK_WIDTH,BACK_HEIGHT+BASE_HEIGHT]);
    }
    
    // screw hole  
    translate([5,-1,(BACK_HEIGHT+BASE_HEIGHT)-5]){
        rotate([-90,0,0]){
            cylinder(r=(SCREW_SHAFT_DIAMETER+CLEARANCE)/2,h=BACK_WIDTH+2);
        }
    }
    
    // magnet holes
    translate([BASE_LENGTH/2-10,BASE_WIDTH/2,1]){
        cylinder(r=(MAGNET_DIAMETER+CLEARANCE)/2, h=MAGNET_DEPTH);
    }
    translate([(BASE_LENGTH/2-10)-((MAGNET_DIAMETER)/2),7,1]){
        cube([MAGNET_DIAMETER,MAGNET_DIAMETER,MAGNET_DEPTH]);
    }
    
    translate([BASE_LENGTH/2+10,BASE_WIDTH/2,1]){
        cylinder(r=(MAGNET_DIAMETER+CLEARANCE)/2, h=MAGNET_DEPTH);
    }
    translate([(BASE_LENGTH/2+10)-((MAGNET_DIAMETER)/2),7,1]){
        cube([MAGNET_DIAMETER,MAGNET_DIAMETER,MAGNET_DEPTH]);
    }
    
    // version
    translate([(BASE_LENGTH/2)-3,1,BASE_HEIGHT+3]){
        rotate([90,0,0]){
            linear_extrude(BASE_WIDTH){
                text(VERSION, size=5);
            }
        }
    }
}

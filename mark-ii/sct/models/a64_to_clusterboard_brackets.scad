/*

Mounting the PINEA64 front-end node to the Clusterboard

Clusterboard dimentions

width: 76mm
depth: 70mm
height: 35mm (incl. SOPINE module)

mounting hole positions (clockwise starting with front-left)
  hole 1 3.65 3.65 inside: 2.92
  hole 2 3.65 8.68
  hole 3 3.6 31.5 
  hole 4 3.60 3.60
  
A64

width: 127
depth: 80
height: 17

mounting hole positions
  hole 1 2.7 (eth. short side) 2.6 inside 3.35
  hole 2 2.6 2.6
  hole 3 2.6 3.0
  hole 4 2.6 3.0
  
*/

color("LIGHTGREEN"){
  Clusterboard();
}

color("LIGHTBLUE"){
  translate([180,10,0]){
    rotate([90,0,90]){
      A64();
    }
  }
}

module A64(){

  BOARD_WIDTH = 127;
  BOARD_DEPTH = 80;
  BOARD_HEIGHT = 3;
  MOUNTING_HOLE_RADIUS = 3.35 / 2;
  MOUNTING_HOLE_OFFSET = MOUNTING_HOLE_RADIUS;
  
  difference(){
  
    // board
    cube([BOARD_WIDTH, BOARD_DEPTH, BOARD_HEIGHT]);
    
    // mounting holes
    translate([2.7+MOUNTING_HOLE_OFFSET, 2.6+MOUNTING_HOLE_OFFSET, -1]){
      cylinder(r=MOUNTING_HOLE_RADIUS, h=6);
    }
    
    // mounting holes
    translate([2.6+MOUNTING_HOLE_OFFSET, BOARD_DEPTH - (2.6+MOUNTING_HOLE_OFFSET), -1]){
      cylinder(r=MOUNTING_HOLE_RADIUS, h=6);
    }
    
    // mounting holes
    translate([BOARD_WIDTH - (2.6+MOUNTING_HOLE_OFFSET), BOARD_DEPTH - (3.0+MOUNTING_HOLE_OFFSET), -1]){
      cylinder(r=MOUNTING_HOLE_RADIUS, h=6);
    }
    
    // mounting holes
    translate([BOARD_WIDTH - (2.6+MOUNTING_HOLE_OFFSET), 3.0+MOUNTING_HOLE_OFFSET, -1]){
      cylinder(r=MOUNTING_HOLE_RADIUS, h=6);
    }
  }

}

module Clusterboard(){

  BOARD_WIDTH = 176;
  BOARD_DEPTH = 170;
  BOARD_HEIGHT = 3;
  MOUNTING_HOLE_RADIUS = 2.92 / 2;
  MOUNTING_HOLE_OFFSET = MOUNTING_HOLE_RADIUS;

  difference(){
    // board
    cube([BOARD_WIDTH,BOARD_DEPTH,BOARD_HEIGHT]);
    
    // mounting holes
    translate([3.65+MOUNTING_HOLE_OFFSET,3.65+MOUNTING_HOLE_OFFSET,-1]){
      cylinder(r=MOUNTING_HOLE_RADIUS, 6);
    }
    
    translate([3.65+MOUNTING_HOLE_OFFSET,BOARD_DEPTH - (8.86+MOUNTING_HOLE_OFFSET),-1]){
      cylinder(r=MOUNTING_HOLE_RADIUS, 6);
    }
    
    translate([BOARD_WIDTH - (3.65+MOUNTING_HOLE_OFFSET),BOARD_DEPTH - (31.5+MOUNTING_HOLE_OFFSET),-1]){
      cylinder(r=MOUNTING_HOLE_RADIUS, 6);
    }
    
    translate([BOARD_WIDTH - (3.65+MOUNTING_HOLE_OFFSET),3.65+MOUNTING_HOLE_OFFSET,-1]){
      cylinder(r=MOUNTING_HOLE_RADIUS, 6);
    }
  }
  
  // modules
  translate([20,15,3]){
    cube([135,85,30]);
  }
  
  // atx power
  translate([176-9.7,83,3]){
    cube([9.7,51.5,13]);
  }
}

// Clusterboard adjustable mounting feet

VERSION = "v1.5";
$fn=50;

CLEARANCE = 1;

// magnets
MAGNET_DIAMETER = 10;
MAGNET_DEPTH = 3;

// m3
SCREW_SHAFT_DIAMETER = 2.9;
SCREW_HEAD_DIAMETER = 5.40;
SCREW_HEAD_DEPTH = 3.5;
NUT_DIAMETER = 6.1;
NUT_DEPTH = 2.3;

// Clusterboard mounting (3 out of 4 holes, one is offset)
ARM_LENGTH = 30;
ARM_THICKNESS = 5;
MIN_BOTTOM_POST_HEIGHT = 10;
MIN_TOP_POST_HEIGHT = 10;
MAX_TOP_POST_DIAMETER = 10;

difference(){
    union(){
        // arm
        cube([ARM_LENGTH, MAX_TOP_POST_DIAMETER, ARM_THICKNESS]);

        // top post
        translate([0,MAX_TOP_POST_DIAMETER/2,0]){
            cylinder(r=MAX_TOP_POST_DIAMETER/2,h=10);
        }


        // bottom post
        translate([ARM_LENGTH,MAX_TOP_POST_DIAMETER/2,-7]){
            cylinder(r=(MAGNET_DIAMETER/2)+1,h=12);
        }
    }
    
    // screw holes
    translate([0,MAX_TOP_POST_DIAMETER/2,-1]){
        cylinder(r=(SCREW_SHAFT_DIAMETER+CLEARANCE)/2, h=12);
    }
    
    translate([0,MAX_TOP_POST_DIAMETER/2,-1]){
        cylinder(r=(SCREW_HEAD_DIAMETER+CLEARANCE)/2, h=SCREW_HEAD_DEPTH+2);
    }
    
    // magnet hole
    translate([ARM_LENGTH,MAX_TOP_POST_DIAMETER/2,-6]){
        cylinder(r=(MAGNET_DIAMETER+CLEARANCE)/2, h=MAGNET_DEPTH);
    }
    translate([ARM_LENGTH-((MAGNET_DIAMETER)/2),7,-6]){
        cube([MAGNET_DIAMETER,MAGNET_DIAMETER,MAGNET_DEPTH]);
    }
    
    // version
    translate([(ARM_LENGTH/2)-5,(MAX_TOP_POST_DIAMETER/2)-3,ARM_THICKNESS-1]){
        linear_extrude(ARM_THICKNESS){
            text(VERSION, size=5);
        }
    }
}

from smbus import SMBus
import subprocess
import time
import os

# config
fe_host_ip = "192.168.1.1"
max_temp = 80.0

# init i2c 
bus = SMBus(1)
PANEL_ADDRESS = 0x20

# enable pins 0-6 for output, 7 input (port A)
bus.write_byte_data(PANEL_ADDRESS, 0x00, 0x80)

# LEDs
# 5 - 0x01 (GPA 0)
# 4 - 0x02 (GPA 1)
# 3 - 0x04 (GPA 2)
# 2 - 0x08 (GPA 3)
# 1 - 0x10 (GPA 4)

# Toggle switch 
# 1 - 0x80 (GPA 7)

def display_status(boot, net, temp, user1, user2):
  status_val = 0x00
  if boot:
    status_val = status_val + 0x10
  if net:
    status_val = status_val + 0x08
  if temp:
    status_val = status_val + 0x04
  if user1:
    status_val = status_val + 0x02
  if user2:
    status_val = status_val + 0x01
  bus.write_byte_data(PANEL_ADDRESS, 0x14, status_val)

def toggle_on():
  hexMask = 0x80
  hexAND7A = hexMask & bus.read_byte_data(PANEL_ADDRESS, 0x12)
  if int(hexAND7A) != 0:
    return True
  else:
    return False

def display_load(load):
  load_val = 0x00
  if load > .2:
    load_val = load_val + 0x10
  if load > .4:
    load_val = load_val + 0x08
  if load > .6:
    load_val = load_val + 0x04
  if load > .8:
    load_val = load_val + 0x02
  if load > 1.0:
    load_val = load_val + 0x01
  bus.write_byte_data(PANEL_ADDRESS, 0x14, load_val)

def get_load():
  s = subprocess.check_output(["uptime"])
  load_split = s.split('load average: ')
  load_one = float(load_split[1].split(',')[0])
  return load_one

def get_boot():
  # always returns true because if we're here, we're booted
  return True

def get_network():
  response = os.system("ping -c 1 " + fe_host_ip)

  if response == 0:
    return True
  else:
    return False

def get_overtemp():
  with open('/sys/class/thermal/thermal_zone0/temp') as f:
    temp = f.readline()
  if float(temp) > max_temp:
    return True
  else:
    return False

def get_user1():
  # TODO: actually do this (whatever it actually is)
  return False

def get_user2():
  # TODO: actually do this
  return False

# loop
while True:
  if toggle_on():
    print "status:"
    display_status(get_boot(), get_network(), get_overtemp(), get_user1(), get_user2())
  else:
    print "load:"
    print get_load()
    display_load(get_load())
  time.sleep(1)

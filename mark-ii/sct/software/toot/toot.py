from mastodon import Mastodon
import os.path

if not os.path.isfile('toot_clientcred.secret'):
  Mastodon.create_app(
    'rainsct',
    api_base_url = 'https://mastodon.technology',
    to_file = 'toot_clientcred.secret'
  ) 

mastodon = Mastodon(
  client_id = 'toot_clientcred.secret',
  api_base_url = 'https://mastodon.technology'
)

mastodon.log_in(
  'rainpsc@2soc.net',
  'secret',
  to_file = 'toot_usercred.secret'
)

mastodon = Mastodon(
  access_token = 'toot_usercred.secret',
  api_base_url = 'https://mastodon.technology'
)
mastodon.toot('Hello from #rainpsc !')

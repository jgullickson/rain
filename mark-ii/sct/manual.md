# Gullickson Laboratories Manual

for the

## RAIN Mark II Supercomputer Trainer

Type 1


## Table of Contents

1. Introduction
2. Assembly Notes
3. Parts List
4. Step-by-Step Assembly
	1. Panel Interface Board
	2. Front-End Interface Board
	3. Front-End Node and Clusterboard Assembly
	3. Cabinet Assembly and Wiring
4. Initial Tests
5. Final Assembly
6. Operation
7. Troubleshooting
8. Specifications
9. Schematics
10. 
# RAIN

*Redundant Array of Inexpensive Nodes*
- or -
*Redundant Array of Independent Nodes*
- or -
*Reliable Accessible Integrated Network*
- or - ...

RAIN is an architecture for open, efficient, and accessible supercomputers.  RAIN's mission is to make supercomputing accessible to a wider and more diverse audience and encourage the development of new, innovating and compelling high-performance applications.

The development of RAIN systems is broken-down into three phases:

## Mark I

[RAIN Mark I](mark-i) (*originally Raiden Mark I*) is a traditional Intel-based, Linux cluster supercomputer.  Consisting of 8 ProLiant DL380 servers and a Gigabit Ethernet interconnect, Mark I serves to establish baseline performance, resource consumption and administrative load for typical distributed-memory supercomputers (albeit at a small scale).

## Mark II

[Mark II](mark-ii) re-creates Mark I using specialized (but still off-the-shelf) parts.  Mark II computers are ARM-based 8-node Linux clusters designed to replicate the environment and performance characteristics of Mark I while improving on it by reducing physical size, power consumption and cost.  Mark II machines are also a test-bed and development platform for system software, developer tools, utilities and operating system facilities designed to make creating high-performance computing applications accessible to a wider-range of programmers, developers, designers and users.

## Mark III

[Mark III](mark-iii) machines extend the performance, features and scale of Mark II machines through custom hardware components (compute modules, application-specific logic, interconnects, etc.).  Mark III will provide a platform on which RAIN's transition from ARM to [RISC-V](https://en.wikipedia.org/wiki/RISC-V) will take place.

## Status

For the most up-to-date information and details on this project, visit [Gullickson Laboratories](https://jasongullickson.com/tags/rain/).


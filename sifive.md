# What is your project idea? (Up to 1,000 words)

I want to make high-performance computing accessible to this generation the way microcomputers made computers themselves accessible to my generation.

To achieve this, I'm developing a scalable, open-source high-performance computing architecture which can be used to build large-scale  supercomputers as well as small-scale, personal supercomputer "trainers".

More than anything else, having a computer of "my own" is what turned me into a programmer.  By producing a small, affordable machine that shares the same architecture as a full-blown supercomputer, aspiring developers can learn how to build high-performance applications without the constraints of time, cost and risk that come with "borrowing time" on a large production system or "renting time" in the cloud.  Once the developer is confident that the application is ready, scaling-up the workload is simply a matter of dispatching it to a larger system, either a dedicated supercomputer or by tapping-in to a "grid" of other trainers.

While it's possible to do some of this now, it requires a lot of specialized knowledge and a wide-range of (often unpredictable) costs.  The system I'm designing is intended to be a self-contained, turn-key system and cost about the same as a desktop computer (sold at a fixed price so there's no surprises no matter how hard you use it).    

Making the open-source is essential to ensuring that the user has complete control, and is able to learn and hone their craft by understanding the machine all the way down to the metal.  I want to provide a platform on which yet unimagined applications can be developed, and I believe that this is only possible when there are no constraints on what a user can do with the system.  This is why an open-source processor architecture is an essential element in my design.



# What is the current status of your project? (Up to 1,000 words)

I'm currently in phase two of a three-phase project to produce the machine described above (A more detailed, if somewhat dated description of the project phases can be found here: https://jjg.2soc.net/2017/10/28/raiden-roadmap-102017/).  The first phase was to build a "traditional" high-performance computing cluster at a small scale to establish a baseline for performance, cost, power consumption, etc.  I call this machine "Mark I" and my work with it was completed about a year ago.

The second phase was to re-create Mark I using off-the-shelf ARM-based single-board computers and compare its various performance parameters against Mark I.  This machine is called Mark II, and I recently completed its initial performance testing.  I also entered this machine in the Hackaday Prize; you can see more details about Mark II on the project page here: https://hackaday.io/project/85392-rain-mark-ii-supercomputer-trainer.

The current status of the project is to finalize the Mark II hardware so that final performance measurements can be made and I can turn my attention to the Mark III phase of the project.  In this phase, I will be expanding on the Mark II machine in both hardware and software dimensions.  On the hardware side, I will be designing new modules to replace the ARM-based compute modules in Mark II, both specialized modules for storage, application-specific logic (FPGA), GPU-style accelerators but also new compute modules based on open processor architectures like RISC-V.



# What is unique about your proposal? (Up to 1,000 words)

The primary difference between my project and other high-performance computers is that I want to create a machine that creates developers; developers who are currently excluded from high-performance computing for one reason or another.  I want to make a machine that allows new people to discover and create new applications.

Most high-performance computing companies create expensive machines with specific objectives.  I want to make a supercomputer that anyone can afford, that anyone can use and that can scale-up when necessary to allow these new applications to utilize exotic and powerful hardware currently reserved for large corporations, government or university research laboratories.

I've written more extensively about why I think this matters here: https://jjg.2soc.net/2017/12/13/why-personal-supercomputers/

Finally, I'm unaware of anyone working on a fully open-source supercomputer (both hardware and software), and that is central to my design.  For reasons I'm sure you're well aware of (security, privacy, flexibility, etc.) I think there is a potentially huge market for an FOSS high-performance computing.


# How can SiFive help? IP, chip design, other? Please be as detailed as possible. (Up to 1,000 words)

I want to make these machines as open as possible.  Part of that is replacing the ARM SOC's with an open design and right now RISC-V is my first choice (https://jjg.2soc.net/2017/12/15/raiden-mark-iii-greater-risc-greater-reward/).  SiFive's Freedom U540 looks like a perfect replacement for the Allwinner A64/R18 SOC's I'm currently using in the Mark II machine.  Having a drop-in replacement for the SOPINE modules I'm using now would make the hardware side of switching from ARM to RISC-V effortless.  The SOPINE module is open-hardware, so it's at least theoretically possible to produce a compatible module based on the Freedom U540, but at the moment I don't have the resources to do this myself.

Beyond having access to a module like this, I can imagine a wide-array of ways my project could benefit from collaboration with SiFive.  I would love to investigate the possibility of creating HPC-oriented RISC-V processor; more cores, integrated FPGA fabric, dedicated vector hardware, etc.

In lieu of the above, having access to an affordable supply of Freedom U540 SOC's would allow me to design my own RISC-V replacement for the SOPINE module once my design abilities reach the necessary level.

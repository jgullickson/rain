# RAIN Mark III

## Summary

Mark III extends the framework of Mark II machines by adding custom hardware components and associated software.

## Status

Research & Development.  Exploratory work is underway in the areas of [RISC-V](https://en.wikipedia.org/wiki/RISC-V), application-specific logic ([FPGA](https://en.wikipedia.org/wiki/Field-programmable_gate_array)) and storage modules based on the SODIMM form-factor used by the PINE64 Clusterboard.  
